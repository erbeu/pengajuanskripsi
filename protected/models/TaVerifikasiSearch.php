<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TaVerifikasi;

/**
 * TaVerifikasiSearch represents the model behind the search form about `app\models\TaVerifikasi`.
 */
class TaVerifikasiSearch extends TaVerifikasi
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_verifikasi', 'id_ajuan'], 'integer'],
            [['tgl_verifikasi', 'hasil_verifikasi', 'catatan_verifikasi', 'npm', 'nama_mahasiswa', 'judul_ajuan', 'nama_admin'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TaVerifikasi::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        $query->joinWith('idAjuan')
            ->joinWith('idAdmin')
            ->join('left join', 'user_mahasiswa', 'ta_ajuan.id_mahasiswa = user_mahasiswa.id_mahasiswa');

        // grid filtering conditions
        $query->andFilterWhere([
            'id_verifikasi' => $this->id_verifikasi,
            'tgl_verifikasi' => $this->tgl_verifikasi,
        ]);

        $query->andFilterWhere(['like', 'hasil_verifikasi', $this->hasil_verifikasi])
            ->andFilterWhere(['like', 'catatan_verifikasi', $this->catatan_verifikasi])
            ->andFilterWhere(['like', 'user_mahasiswa.npm', $this->npm])
            ->andFilterWhere(['like', 'user_mahasiswa.nama_mahasiswa', $this->nama_mahasiswa])
            ->andFilterWhere(['like', 'ta_ajuan.judul_ajuan', $this->judul_ajuan])
            ->andFilterWhere(['like', 'user_admin', $this->nama_admin]);

        return $dataProvider;
    }
}
