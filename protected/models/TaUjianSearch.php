<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TaUjian;

/**
 * TaUjianSearch represents the model behind the search form about `app\models\TaUjian`.
 */
class TaUjianSearch extends TaUjian
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_detail', 'id_ajuan'], 'integer'],
            [['catatan_penguji'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $options)
    {
        $query = TaUjian::find()
            ->where(['id_ajuan' => $options['id_ajuan']]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_detail' => $this->id_detail,
            'id_ajuan' => $this->id_ajuan,
        ]);

        $query->andFilterWhere(['like', 'catatan_penguji', $this->catatan_penguji]);

        return $dataProvider;
    }
}
