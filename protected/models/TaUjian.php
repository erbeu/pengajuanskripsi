<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ta_ujian".
 *
 * @property integer $id_detail
 * @property integer $id_ajuan
 * @property string $catatan_penguji
 *
 * @property UserPenguji $idPenguji
 * @property TaAjuan $idAjuan
 */
class TaUjian extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ta_ujian';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_ajuan', 'id_detail', 'catatan_penguji'], 'required'],
            [['id_ajuan', 'id_detail', 'pemberi_hasil'], 'integer'],
            [['catatan_penguji'], 'string'],
            [['id_detail'], 'exist', 'skipOnError' => true, 'targetClass' => DetailUjian::className(), 'targetAttribute' => ['id_detail' => 'id_detail']],
            [['id_ajuan'], 'exist', 'skipOnError' => true, 'targetClass' => TaAjuan::className(), 'targetAttribute' => ['id_ajuan' => 'id_ajuan']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_ujian' => 'Id Ujian',
            'id_ajuan' => 'Id Ajuan',
            'catatan_penguji' => 'Catatan Penguji',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDetailUjian()
    {
        return $this->hasOne(DetailUjian::className(), ['id_detail' => 'id_detail']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdAjuan()
    {
        return $this->hasOne(TaAjuan::className(), ['id_ajuan' => 'id_ajuan']);
    }
}
