<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TaAjuan;
use app\models\User;
use app\models\UserMahasiswa;

/**
 * TaAjuanSearch represents the model behind the search form about `app\models\TaAjuan`.
 */
class TaAjuanSearch extends TaAjuan
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_ajuan'], 'integer'],
            [['npm', 'id_mahasiswa', 'judul_ajuan', 'tgl_ajuan'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $id_mahasiswa)
    {
        $role = Yii::$app->user->identity->role;
        $condition = [];
        
        if($role == User::ROLE_ADMIN) {
            $condition = ['ta_ajuan.id_mahasiswa' => $id_mahasiswa];
        } elseif($role == User::ROLE_PENGUJI) {
            $condition = ['ta_ajuan.id_mahasiswa' => $id_mahasiswa, 'hasil_verifikasi' => 'diterima'];
        } elseif($role == User::ROLE_MAHASISWA) {
            $id_mahasiswa = UserMahasiswa::findOne(['id_user' => Yii::$app->user->identity->id])->id_mahasiswa;
            $condition = ['ta_ajuan.id_mahasiswa' => $id_mahasiswa];
        }
        
        $query = TaAjuan::find()->joinWith('idMahasiswa')->where($condition);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if(!empty($params)) {
            $this->load($params);
        } else {
            $query->orderBy('tgl_ajuan DESC');
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        // grid filtering conditions
        $query->andFilterWhere([
            'id_ajuan' => $this->id_ajuan,
            'tgl_ajuan' => $this->tgl_ajuan,
        ]);

        $query->andFilterWhere(['like', 'judul_ajuan', $this->judul_ajuan])
            ->andFilterWhere(['like', 'user_mahasiswa.npm', $this->npm])
            ->andFilterWhere(['like', 'user_mahasiswa.nama_mahasiswa', $this->id_mahasiswa]);

        return $dataProvider;
    }
}
