<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use app\models\UserMahasiswa;
use app\models\UserPenguji;
use app\models\TaAjuan;

/**
 * UserMahasiswaSearch represents the model behind the search form about `app\models\UserMahasiswa`.
 */
class UserMahasiswaSearch extends UserMahasiswa
{
    public $tgl_ujian, $ruang;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_mahasiswa', 'id_user', 'semester'], 'integer'],
            [['npm', 'nama_mahasiswa', 'jenis_kelamin', 'kelas', 'tahun_masuk', 'prodi', 'tgl_ujian', 'ruang'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserMahasiswa::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if(!empty($params)) {
            $this->load($params);
        } else {
            $query->orderBy('id_mahasiswa DESC');
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_mahasiswa' => $this->id_mahasiswa,
            'id_user' => $this->id_user,
            'semester' => $this->semester,
            'tahun_masuk' => $this->tahun_masuk,
        ]);

        $query->andFilterWhere(['like', 'npm', $this->npm])
            ->andFilterWhere(['like', 'nama_mahasiswa', $this->nama_mahasiswa])
            ->andFilterWhere(['like', 'jenis_kelamin', $this->jenis_kelamin])
            ->andFilterWhere(['like', 'kelas', $this->kelas])
            ->andFilterWhere(['like', 'prodi', $this->prodi]);

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchAdminTa($params, $options)
    {
        $query = UserMahasiswa::find();
        
        if($options['page'] == 'ta-mahasiswa') {
            /*
            $sub = TaAjuan::find()->select(['id_mahasiswa'])->where(['hasil_verifikasi' => Yii::$app->params['hasil_verifikasi'][0]]);
            $query = UserMahasiswa::find()
                ->where(['id_mahasiswa' => $sub]);
                */
            $query = UserMahasiswa::find()
                ->joinWith(['taAjuans'])
                ->groupBy('user_mahasiswa.id_mahasiswa')
                ->where(['ta_ajuan.submit' => 1, 'ta_ajuan.hasil_verifikasi' => Yii::$app->params['hasil_verifikasi'][0]]);
        } elseif($options['page'] == 'ta-mahasiswa-arsip') {
            $query = UserMahasiswa::find()
                ->joinWith(['taAjuans'])
                ->groupBy('user_mahasiswa.id_mahasiswa')
                ->where(['ta_ajuan.submit' => 0, 'ta_ajuan.hasil_verifikasi' => Yii::$app->params['hasil_verifikasi'][0]]);
        } elseif($options['page'] == 'ta-lolos-verifikasi') {
            $query = UserMahasiswa::find()
                ->joinWith(['detailUjian', 'taAjuans', 'taAjuans.taVerifikasi'])
                ->where(['ta_verifikasi.submit' => 1, 'ta_ajuan.hasil_verifikasi' => Yii::$app->params['hasil_verifikasi'][1]])
                ->groupBy('user_mahasiswa.id_mahasiswa')
                ->having('count(ta_ajuan.hasil_verifikasi) = 3 AND count(detail_ujian.id_mahasiswa)/2 < 3');
        } elseif($options['page'] == 'ta-pra-uji') {
            $query = UserMahasiswa::find()
                ->joinWith(['detailUjian', 'taAjuans'])
                ->join('LEFT JOIN', 'ta_ujian', 'ta_ujian.id_ajuan = ta_ajuan.id_ajuan AND ta_ujian.id_detail = detail_ujian.id_detail')
                ->where([
                    'ta_ajuan.hasil_verifikasi' => Yii::$app->params['hasil_verifikasi'][1],
                    //'ta_ajuan.hasil_ujian' => Yii::$app->params['hasil_ujian'][0]
                ])
                ->groupBy('user_mahasiswa.id_mahasiswa')
                ->having('count(ta_ajuan.hasil_verifikasi)/2 = 3 AND count(detail_ujian.id_mahasiswa)/2 = 3 AND count(ta_ujian.catatan_penguji)/2 < 3');
        } elseif($options['page'] == 'jadwal-ujian') {
            $query = UserMahasiswa::find()
                ->joinWith(['detailUjian', 'taAjuans'])
                ->where(['detail_ujian.id_penguji' => $options['id_penguji']])
                ->andWhere(['ta_ajuan.hasil_verifikasi' => Yii::$app->params['hasil_verifikasi'][1]])
                ->andWhere(['ta_ajuan.hasil_ujian' => Yii::$app->params['hasil_ujian'][0]]);
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        $dataProvider->sort->attributes['tgl_ujian'] = [
            'asc' => ['detail_ujian.tgl_ujian' => SORT_ASC],
            'desc' => ['detail_ujian.tgl_ujian' => SORT_DESC],
        ];
        
        $dataProvider->sort->attributes['ruang'] = [
            'asc' => ['detail_ujian.ruang' => SORT_ASC],
            'desc' => ['detail_ujian.ruang' => SORT_DESC],
        ];

        // grid filtering conditions
        $query->andFilterWhere([
            'id_mahasiswa' => $this->id_mahasiswa,
            'id_user' => $this->id_user,
            'semester' => $this->semester,
            'tahun_masuk' => $this->tahun_masuk,
        ]);

        $query->andFilterWhere(['like', 'npm', $this->npm])
            ->andFilterWhere(['like', 'nama_mahasiswa', $this->nama_mahasiswa])
            ->andFilterWhere(['like', 'jenis_kelamin', $this->jenis_kelamin])
            ->andFilterWhere(['like', 'kelas', $this->kelas])
            ->andFilterWhere(['like', 'prodi', $this->prodi])
            ->andFilterWhere(['like', 'detail_ujian.tgl_ujian', $this->tgl_ujian])
            ->andFilterWhere(['like', 'detail_ujian.ruang', $this->ruang]);

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchPengujiTa($params, $options)
    {
        $penguji = UserPenguji::findOne(['id_user' => Yii::$app->user->identity->id_user]);
        
        if(empty($options['page'])) {
            $query = UserMahasiswa::find()
                //->joinWith(['detailUjian', 'taAjuans'])
                ->join('INNER JOIN', 'ta_ajuan', 'ta_ajuan.id_mahasiswa = user_mahasiswa.id_mahasiswa')
                ->join('LEFT JOIN', 'detail_ujian', 'detail_ujian.id_mahasiswa = user_mahasiswa.id_mahasiswa')
                ->join('LEFT JOIN', 'ta_ujian', 'ta_ujian.id_detail = detail_ujian.id_detail AND ta_ujian.id_ajuan = ta_ajuan.id_ajuan')
                ->where(['detail_ujian.id_penguji' => $penguji->id_penguji])
                ->andWhere(['ta_ajuan.hasil_verifikasi' => Yii::$app->params['hasil_verifikasi'][1]])
                ->andWhere('ta_ujian.id_detail is null')
                ->distinct();
        } elseif($options['page'] == 'arsip') {
            $query = UserMahasiswa::find()
                //->joinWith(['detailUjian', 'taAjuans'])
                ->join('INNER JOIN', 'ta_ajuan', 'ta_ajuan.id_mahasiswa = user_mahasiswa.id_mahasiswa')
                ->join('LEFT JOIN', 'detail_ujian', 'detail_ujian.id_mahasiswa = user_mahasiswa.id_mahasiswa')
                ->join('LEFT JOIN', 'ta_ujian', 'ta_ujian.id_detail = detail_ujian.id_detail AND ta_ujian.id_ajuan = ta_ajuan.id_ajuan')
                ->where(['detail_ujian.id_penguji' => $penguji->id_penguji])
                ->andWhere(['ta_ajuan.hasil_verifikasi' => Yii::$app->params['hasil_verifikasi'][1]])
                ->andWhere('ta_ujian.id_detail is not null')
                ->distinct();
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_mahasiswa' => $this->id_mahasiswa,
            'id_user' => $this->id_user,
            'semester' => $this->semester,
            'tahun_masuk' => $this->tahun_masuk,
        ]);

        $query->andFilterWhere(['like', 'npm', $this->npm])
            ->andFilterWhere(['like', 'nama_mahasiswa', $this->nama_mahasiswa])
            ->andFilterWhere(['like', 'jenis_kelamin', $this->jenis_kelamin])
            ->andFilterWhere(['like', 'kelas', $this->kelas])
            ->andFilterWhere(['like', 'prodi', $this->prodi]);

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchLaporanTa($params, $options)
    {
        $query = UserMahasiswa::find();
        if($options['page'] == 'ta-lolos-verifikasi') {
            $query = UserMahasiswa::find()
                ->select([
                    'user_mahasiswa.id_mahasiswa',
                    'user_mahasiswa.npm',
                    'user_mahasiswa.nama_mahasiswa',
                    'user_mahasiswa.jenis_kelamin',
                    'user_mahasiswa.kelas',
                    'user_mahasiswa.tahun_masuk',
                    'user_mahasiswa.prodi',
                    "SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(`judul_ajuan`), ',', 1), ',', -1) AS judul_1",
                    "SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(`judul_ajuan`), ',', 2), ',', -1) AS judul_2",
                    "SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(`judul_ajuan`), ',', 3), ',', -1) AS judul_3",
                ])
                ->joinWith(['taAjuans'])
                ->where(['ta_ajuan.hasil_verifikasi' => 'diterima'])
                ->groupBy('user_mahasiswa.id_mahasiswa')
                ->having('COUNT(id_ajuan) = 3');
        } elseif($options['page'] == 'ta-tidak-lolos-verifikasi') {
            $query = UserMahasiswa::find()
                ->select([
                    'user_mahasiswa.id_mahasiswa',
                    'user_mahasiswa.npm',
                    'user_mahasiswa.nama_mahasiswa',
                    'user_mahasiswa.jenis_kelamin',
                    'user_mahasiswa.kelas',
                    'user_mahasiswa.tahun_masuk',
                    'user_mahasiswa.prodi',
                    'ta_ajuan.judul_ajuan as judul',
                ])
                ->joinWith(['taAjuans'])
                ->where(['ta_ajuan.hasil_verifikasi' => 'ditolak']);
        } elseif($options['page'] == 'ta-lolos-uji') {
            $query = UserMahasiswa::find()
                ->select([
                    'user_mahasiswa.id_mahasiswa',
                    'user_mahasiswa.id_user',
                    'user_mahasiswa.npm',
                    'user_mahasiswa.nama_mahasiswa',
                    'user_mahasiswa.jenis_kelamin',
                    'user_mahasiswa.kelas',
                    'user_mahasiswa.semester',
                    'user_mahasiswa.tahun_masuk',
                    'user_mahasiswa.prodi',
                    'ta_ajuan.judul_ajuan judul',
                    'ta_ajuan.hasil_ujian',
                    "SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(user_penguji.nama_penguji SEPARATOR '<@@>'),'<@@>',1),'<@@>',-1) penguji_1",
                    "SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(user_penguji.nama_penguji SEPARATOR '<@@>'),'<@@>',2),'<@@>',-1) penguji_2",
                    "SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(ta_ujian.catatan_penguji SEPARATOR '<@@>'),'<@@>',1),'<@@>',-1) catatan_penguji_1",
                    "SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(ta_ujian.catatan_penguji SEPARATOR '<@@>'),'<@@>',2),'<@@>',-1) catatan_penguji_2"
                ])
                ->join('INNER JOIN', 'ta_ajuan', 'ta_ajuan.id_mahasiswa = user_mahasiswa.id_mahasiswa')
                ->join('LEFT JOIN', 'detail_ujian', 'detail_ujian.id_mahasiswa = user_mahasiswa.id_mahasiswa')
                ->join('LEFT JOIN', 'ta_ujian', 'ta_ujian.id_detail = detail_ujian.id_detail AND ta_ujian.id_ajuan = ta_ajuan.id_ajuan')
                ->join('INNER JOIN', 'user_penguji', 'detail_ujian.id_penguji = user_penguji.id_penguji')
                ->where(['ta_ajuan.hasil_ujian' => Yii::$app->params['hasil_ujian'][1]])
                ->groupBy(['user_mahasiswa.id_mahasiswa', 'ta_ajuan.judul_ajuan'])
                ->having('COUNT(ta_ujian.catatan_penguji) = 2');
        } elseif($options['page'] == 'ta-tidak-lolos-uji') {
            $query = UserMahasiswa::find()
                ->select([
                    'user_mahasiswa.id_mahasiswa',
                    'user_mahasiswa.id_user',
                    'user_mahasiswa.npm',
                    'user_mahasiswa.nama_mahasiswa',
                    'user_mahasiswa.jenis_kelamin',
                    'user_mahasiswa.kelas',
                    'user_mahasiswa.semester',
                    'user_mahasiswa.tahun_masuk',
                    'user_mahasiswa.prodi',
                    'ta_ajuan.judul_ajuan judul',
                    'ta_ajuan.hasil_ujian',
                    "SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(user_penguji.nama_penguji SEPARATOR '<@@>'),'<@@>',1),'<@@>',-1) penguji_1",
                    "SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(user_penguji.nama_penguji SEPARATOR '<@@>'),'<@@>',2),'<@@>',-1) penguji_2",
                    "SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(ta_ujian.catatan_penguji SEPARATOR '<@@>'),'<@@>',1),'<@@>',-1) catatan_penguji_1",
                    "SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(ta_ujian.catatan_penguji SEPARATOR '<@@>'),'<@@>',2),'<@@>',-1) catatan_penguji_2"
                ])
                ->join('INNER JOIN', 'ta_ajuan', 'ta_ajuan.id_mahasiswa = user_mahasiswa.id_mahasiswa')
                ->join('LEFT JOIN', 'detail_ujian', 'detail_ujian.id_mahasiswa = user_mahasiswa.id_mahasiswa')
                ->join('LEFT JOIN', 'ta_ujian', 'ta_ujian.id_detail = detail_ujian.id_detail AND ta_ujian.id_ajuan = ta_ajuan.id_ajuan')
                ->join('INNER JOIN', 'user_penguji', 'detail_ujian.id_penguji = user_penguji.id_penguji')
                ->where(['ta_ajuan.hasil_ujian' => Yii::$app->params['hasil_ujian'][2]])
                ->groupBy(['user_mahasiswa.id_mahasiswa', 'ta_ajuan.judul_ajuan'])
                ->having('COUNT(ta_ujian.catatan_penguji) = 2');
        } elseif($options['page'] == 'mahasiswa') {
            $query = UserMahasiswa::find()
                ->select([
                    'user_mahasiswa.id_mahasiswa',
                    'user_mahasiswa.npm',
                    'user_mahasiswa.nama_mahasiswa',
                    'user_mahasiswa.jenis_kelamin',
                    'user_mahasiswa.kelas',
                    'user_mahasiswa.tahun_masuk',
                    'user_mahasiswa.prodi',
                    'count(ta_ajuan.judul_ajuan) as jumlah_judul',
                ])
                ->joinWith(['taAjuans'])
                ->groupBy('user_mahasiswa.id_mahasiswa');
        } elseif($options['page'] == 'laporan-penguji-detail') {
            $query = UserMahasiswa::find()
                ->select([
                    'user_mahasiswa.id_mahasiswa',
                    'user_mahasiswa.npm',
                    'user_mahasiswa.nama_mahasiswa',
                    'user_mahasiswa.jenis_kelamin',
                    'user_mahasiswa.tahun_masuk',
                    'user_mahasiswa.prodi',
                    'ta_ajuan.judul_ajuan',
                    'ta_ajuan.hasil_ujian',
                    'detail_ujian.tgl_ujian',
                    'ta_ujian.catatan_penguji'
                ])
                ->join('INNER JOIN', 'ta_ajuan', 'ta_ajuan.id_mahasiswa = user_mahasiswa.id_mahasiswa')
                ->join('LEFT JOIN', 'detail_ujian', 'detail_ujian.id_mahasiswa = user_mahasiswa.id_mahasiswa')
                ->join('LEFT JOIN', 'ta_ujian', 'ta_ujian.id_detail = detail_ujian.id_detail AND ta_ujian.id_ajuan = ta_ajuan.id_ajuan')
                ->where(['detail_ujian.id_penguji' => $options['id_penguji']])
                ->andWhere(['YEAR(detail_ujian.tgl_ujian)' => $options['tahun']])
                ->andWhere(['not', ['ta_ajuan.hasil_ujian' => Yii::$app->params['hasil_ujian'][0]]]);
        }

        // add conditions that should always apply here

        /*$dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);*/

        $this->load($params);

        // grid filtering conditions
        $query->andFilterWhere([
            'id_mahasiswa' => $this->id_mahasiswa,
            'id_user' => $this->id_user,
            'semester' => $this->semester,
            'tahun_masuk' => $this->tahun_masuk,
        ]);

        $query->andFilterWhere(['like', 'npm', $this->npm])
            ->andFilterWhere(['like', 'nama_mahasiswa', $this->nama_mahasiswa])
            ->andFilterWhere(['like', 'kelas', $this->kelas])
            ->andFilterWhere(['like', 'prodi', $this->prodi]);

        $count = Yii::$app->db->createCommand('
            SELECT COUNT(*) FROM ('.$query->createCommand()->getRawSql().
        ') c')->queryScalar();
        $dataProvider = new SqlDataProvider([
            'sql' => $query->createCommand()->getRawSql(),
            'totalCount' => $count,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        return $dataProvider;
    }
}
