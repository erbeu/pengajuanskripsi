<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "judul_final".
 *
 * @property integer $id_judul
 * @property integer $id_ajuan
 * @property string $npm
 * @property string $nama_mahasiswa
 * @property string $kelas
 * @property string $tahun_masuk
 * @property string $prodi
 * @property string $judul
 * @property string $tahun_ajuan
 *
 * @property TaAjuan $idAjuan
 */
class JudulFinal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'judul_final';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_ajuan', 'npm', 'nama_mahasiswa', 'jenis_kelamin', 'kelas', 'tahun_masuk', 'prodi', 'judul', 'tahun_ajuan'], 'required'],
            [['id_ajuan'], 'integer'],
            [['tahun_masuk', 'tahun_ajuan'], 'safe'],
            [['npm', 'jenis_kelamin', 'kelas'], 'string', 'max' => 10],
            [['nama_mahasiswa', 'prodi'], 'string', 'max' => 50],
            [['judul'], 'string', 'max' => 255],
            [['id_ajuan'], 'exist', 'skipOnError' => true, 'targetClass' => TaAjuan::className(), 'targetAttribute' => ['id_ajuan' => 'id_ajuan']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_judul' => 'Id Judul',
            'id_ajuan' => 'Id Ajuan',
            'npm' => 'Npm',
            'nama_mahasiswa' => 'Nama Mahasiswa',
            'jenis_kelamin' => 'Jenis Kelamin',
            'kelas' => 'Kelas',
            'tahun_masuk' => 'Tahun Masuk',
            'prodi' => 'Prodi',
            'judul' => 'Judul',
            'tahun_ajuan' => 'Tahun Ajuan',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdAjuan()
    {
        return $this->hasOne(TaAjuan::className(), ['id_ajuan' => 'id_ajuan']);
    }
}
