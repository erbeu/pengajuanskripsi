<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\DetailUjian;

/**
 * DetailUjianSearch represents the model behind the search form about `app\models\DetailUjian`.
 */
class DetailUjianSearch extends DetailUjian
{
    public $nama_mahasiswa, $nama_penguji;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_detail', 'id_admin'], 'integer'],
            [['tgl_ujian', 'ruang', 'nama_penguji', 'nama_mahasiswa'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $id_mahasiswa)
    {
        $query = DetailUjian::find()->where(['detail_ujian.id_mahasiswa' => $id_mahasiswa]);

        // add conditions that should always apply here
        
        $query->orderBy('tgl_ujian DESC');
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith(['idPenguji', 'idMahasiswa']);
        
        $dataProvider->sort->attributes['nama_mahasiswa'] = [
            'asc' => ['user_mahasiswa.nama_mahasiswa' => SORT_ASC],
            'desc' => ['user_mahasiswa.nama_mahasiswa' => SORT_DESC],
        ];
        
        $dataProvider->sort->attributes['nama_penguji'] = [
            'asc' => ['user_penguji.nama_penguji' => SORT_ASC],
            'desc' => ['user_penguji.nama_penguji' => SORT_DESC],
        ];
        
        // grid filtering conditions
        $query->andFilterWhere([
            'id_detail' => $this->id_detail,
            'tgl_ujian' => $this->tgl_ujian,
            'id_admin' => $this->id_admin, 
        ]);

        $query->andFilterWhere(['like', 'ruang', $this->ruang])
            ->andFilterWhere(['like', 'user_penguji.nama_penguji', $this->nama_penguji])
            ->andFilterWhere(['like', 'user_mahasiswa.nama_mahasiswa', $this->nama_mahasiswa]);

        return $dataProvider;
    }
}
