<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\JudulFinal;

/**
 * JudulFinalSearch represents the model behind the search form about `app\models\JudulFinal`.
 */
class JudulFinalSearch extends JudulFinal
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_judul', 'id_ajuan'], 'integer'],
            [['npm', 'nama_mahasiswa', 'jenis_kelamin', 'kelas', 'tahun_masuk', 'prodi', 'judul', 'tahun_ajuan'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = JudulFinal::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_judul' => $this->id_judul,
            'id_ajuan' => $this->id_ajuan,
            'tahun_masuk' => $this->tahun_masuk,
            'tahun_ajuan' => $this->tahun_ajuan,
        ]);

        $query->andFilterWhere(['like', 'npm', $this->npm])
            ->andFilterWhere(['like', 'nama_mahasiswa', $this->nama_mahasiswa])
            ->andFilterWhere(['like', 'jenis_kelamin', $this->jenis_kelamin])
            ->andFilterWhere(['like', 'kelas', $this->kelas])
            ->andFilterWhere(['like', 'prodi', $this->prodi])
            ->andFilterWhere(['like', 'judul', $this->judul]);

        return $dataProvider;
    }
}
