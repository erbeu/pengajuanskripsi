<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_mahasiswa".
 *
 * @property integer $id_mahasiswa
 * @property integer $id_user
 * @property string $npm
 * @property string $nama_mahasiswa
 * @property string $kelas
 * @property integer $semester
 * @property string $tahun_masuk
 * @property string $prodi
 *
 * @property TaAjuan[] $taAjuans
 * @property User $idUser
 */
class UserMahasiswa extends \yii\db\ActiveRecord
{
    public $judul, $judul_1, $judul_2, $judul_3, $jumlah_judul;
    public $hasil_ujian, $penguji_1, $penguji_2, $catatan_penguji_1, $catatan_penguji_2;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_mahasiswa';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'nama_mahasiswa', 'jenis_kelamin', 'kelas', 'semester', 'tahun_masuk', 'prodi'], 'required'],
            [['npm'], 'required', 'on' => 'create'],
            [['id_user', 'semester'], 'integer'],
            [['tahun_masuk'], 'safe'],
            [['npm', 'kelas', 'jenis_kelamin'], 'string', 'max' => 10],
            [['nama_mahasiswa', 'prodi'], 'string', 'max' => 50],
            [['npm'], 'unique'],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id_user']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_mahasiswa' => 'Id Mahasiswa',
            'id_user' => 'Id User',
            'npm' => 'Npm',
            'nama_mahasiswa' => 'Nama Mahasiswa',
            'jenis_kelamin' => 'Jenis Kelamin',
            'kelas' => 'Kelas',
            'semester' => 'Semester',
            'tahun_masuk' => 'Tahun Masuk',
            'prodi' => 'Prodi',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaAjuans()
    {
        return $this->hasMany(TaAjuan::className(), ['id_mahasiswa' => 'id_mahasiswa']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDetailUjian()
    {
        return $this->hasMany(DetailUjian::className(), ['id_mahasiswa' => 'id_mahasiswa']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUser()
    {
        return $this->hasOne(User::className(), ['id_user' => 'id_user']);
    }
}
