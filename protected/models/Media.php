<?php
namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;

class Media extends Model
{
    /**
     * @var UploadedFile
     */
    public $imageFile;

    public function rules()
    {
        return [
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'],
        ];
    }
    
    public function upload()
    {
        if ($this->validate()) {
            $filename = date('YmdGis').'-'.rand(0,99).'-'.$this->imageFile->baseName;
            $this->imageFile->saveAs('uploads/' . $filename . '.' . $this->imageFile->extension);
            return true;
        } else {
            return false;
        }
    }
}