<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\UserPenguji;

/**
 * UserPengujiSearch represents the model behind the search form about `app\models\UserPenguji`.
 */
class UserPengujiSearch extends UserPenguji
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_penguji', 'id_user', 'tahun'], 'integer'],
            [['nik', 'nama_penguji', 'jenis_kelamin', 'jafung'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserPenguji::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_penguji' => $this->id_penguji,
            'id_user' => $this->id_user,
        ]);

        $query->andFilterWhere(['like', 'nik', $this->nik])
            ->andFilterWhere(['like', 'nama_penguji', $this->nama_penguji])
            ->andFilterWhere(['like', 'jenis_kelamin', $this->jenis_kelamin])
            ->andFilterWhere(['like', 'jafung', $this->jafung]);

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchLaporan($params)
    {
        $query = UserPenguji::find()
            ->select([
                'user_penguji.id_penguji',
                'user_penguji.nik',
                'user_penguji.nama_penguji',
                'user_penguji.jenis_kelamin',
                'user_penguji.jafung',
                'round(count(ta_ujian.id_detail)/3) AS jumlah_mahasiswa',
                'year(detail_ujian.tgl_ujian) as tahun'
            ])
            ->join('INNER JOIN', 'detail_ujian', 'detail_ujian.id_penguji = user_penguji.id_penguji')
            ->join('INNER JOIN', 'ta_ujian', 'ta_ujian.id_detail = detail_ujian.id_detail')
            ->groupBy(['user_penguji.id_penguji', 'year(detail_ujian.tgl_ujian)'])
            ->having('count(ta_ujian.id_detail)%3 = 0');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        $dataProvider->sort->attributes['jumlah_mahasiswa'] = [
            'asc' => ['round(count(ta_ujian.id_detail)/3) AS jumlah_mahasiswa' => SORT_ASC],
            'desc' => ['round(count(ta_ujian.id_detail)/3) AS jumlah_mahasiswa' => SORT_DESC],
        ];
        
        $dataProvider->sort->attributes['tahun'] = [
            'asc' => ['year(detail_ujian.tgl_ujian)' => SORT_ASC],
            'desc' => ['year(detail_ujian.tgl_ujian)' => SORT_DESC],
        ];

        // grid filtering conditions
        $query->andFilterWhere([
            'id_penguji' => $this->id_penguji,
            'id_user' => $this->id_user,
            'year(detail_ujian.tgl_ujian)' => $this->tahun,
        ]);

        $query->andFilterWhere(['like', 'nik', $this->nik])
            ->andFilterWhere(['like', 'nama_penguji', $this->nama_penguji])
            ->andFilterWhere(['like', 'jafung', $this->jafung]);

        return $dataProvider;
    }
}
