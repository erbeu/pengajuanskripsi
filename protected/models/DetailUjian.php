<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "detail_ujian".
 *
 * @property integer $id_ujian
 * @property integer $id_penguji
 * @property integer $id_mahasiswa
 * @property string $tgl_ujian
 * @property string $ruang
 * @property integer $id_admin
 *
 * @property UserPenguji $idPenguji
 * @property UserMahasiswa $idMahasiswa
 */
class DetailUjian extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'detail_ujian';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_penguji', 'id_mahasiswa', 'tgl_ujian', 'ruang', 'id_admin'], 'required'],
            [['id_penguji', 'id_mahasiswa', 'id_admin'], 'integer'],
            [['tgl_ujian'], 'safe'],
            [['ruang'], 'string', 'max' => 10],
            [['id_penguji'], 'exist', 'skipOnError' => true, 'targetClass' => UserPenguji::className(), 'targetAttribute' => ['id_penguji' => 'id_penguji']],
            [['id_mahasiswa'], 'exist', 'skipOnError' => true, 'targetClass' => UserMahasiswa::className(), 'targetAttribute' => ['id_mahasiswa' => 'id_mahasiswa']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_detail' => 'Id Detail',
            'id_penguji' => 'Nama Penguji',
            'id_mahasiswa' => 'Nama Mahasiswa',
            'tgl_ujian' => 'Tgl Ujian',
            'ruang' => 'Ruang',
            'id_admin' => 'Id Admin',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPenguji()
    {
        return $this->hasOne(UserPenguji::className(), ['id_penguji' => 'id_penguji']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdMahasiswa()
    {
        return $this->hasOne(UserMahasiswa::className(), ['id_mahasiswa' => 'id_mahasiswa']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdAdmin()
    {
        return $this->hasOne(UserAdmin::className(), ['id_admin' => 'id_admin']);
    }
}
