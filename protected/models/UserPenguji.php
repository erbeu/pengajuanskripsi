<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_penguji".
 *
 * @property integer $id_penguji
 * @property integer $id_user
 * @property string $nik
 * @property string $nama_penguji
 * @property string $jafung
 *
 * @property DetailUjian[] $rincianUjians
 * @property User $idUser
 */
class UserPenguji extends \yii\db\ActiveRecord
{
    public $jumlah_mahasiswa;
    public $tahun;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_penguji';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'nik', 'nama_penguji', 'jenis_kelamin', 'jafung'], 'required'],
            [['id_user'], 'integer'],
            [['nik', 'jenis_kelamin'], 'string', 'max' => 10],
            [['nama_penguji'], 'string', 'max' => 100],
            [['jafung'], 'string', 'max' => 50],
            [['nik'], 'unique'],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id_user']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_penguji' => 'Id Penguji',
            'id_user' => 'Id User',
            'nik' => 'Nik',
            'nama_penguji' => 'Nama Penguji',
            'jenis_kelamin' => 'Jenis Kelamin',
            'jafung' => 'Jafung',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDetailUjian()
    {
        return $this->hasMany(DetailUjian::className(), ['id_penguji' => 'id_penguji']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUser()
    {
        return $this->hasOne(User::className(), ['id_user' => 'id_user']);
    }
}
