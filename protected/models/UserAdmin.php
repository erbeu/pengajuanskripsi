<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_admin".
 *
 * @property integer $id_admin
 * @property integer $id_user
 * @property string $nama_admin
 * @property string $jabatan
 *
 * @property TaKandidat[] $taKandidats
 * @property User $idUser
 */
class UserAdmin extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_admin';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'nama_admin', 'jenis_kelamin', 'jabatan'], 'required'],
            [['id_user'], 'integer'],
            [['nama_admin', 'jenis_kelamin', 'jabatan'], 'string', 'max' => 50],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id_user']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_admin' => 'Id Admin',
            'id_user' => 'Username',
            'nama_admin' => 'Nama Admin',
            'jenis_kelamin' => 'Jenis Kelamin',
            'jabatan' => 'Jabatan',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaKandidats()
    {
        return $this->hasMany(TaKandidat::className(), ['id_admin' => 'id_admin']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUser()
    {
        return $this->hasOne(User::className(), ['id_user' => 'id_user']);
    }
}
