<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ta_verifikasi".
 *
 * @property integer $id_verifikasi
 * @property integer $id_admin
 * @property integer $id_ajuan
 * @property string $tgl_verifikasi
 * @property string $hasil_verifikasi
 * @property string $catatan_verifikasi
 *
 * @property UserAdmin $idAdmin
 * @property TaAjuan $idAjuan
 */
class TaVerifikasi extends \yii\db\ActiveRecord
{
    public $nama_mahasiswa;
    public $npm;
    public $judul_ajuan;
    public $nama_admin;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ta_verifikasi';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_admin', 'id_ajuan', 'tgl_verifikasi', 'catatan_verifikasi'], 'required'],
            [['id_admin', 'id_ajuan'], 'integer'],
            [['tgl_verifikasi'], 'safe'],
            [['catatan_verifikasi'], 'string'],
            [['id_admin'], 'exist', 'skipOnError' => true, 'targetClass' => UserAdmin::className(), 'targetAttribute' => ['id_admin' => 'id_admin']],
            [['id_ajuan'], 'exist', 'skipOnError' => true, 'targetClass' => TaAjuan::className(), 'targetAttribute' => ['id_ajuan' => 'id_ajuan']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_verifikasi' => 'Id Verifikasi',
            'id_admin' => 'Id Admin',
            'id_ajuan' => 'Id Ajuan',
            'tgl_verifikasi' => 'Tgl Verifikasi',
            'catatan_verifikasi' => 'Catatan Verifikasi',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdAdmin()
    {
        return $this->hasOne(UserAdmin::className(), ['id_admin' => 'id_admin']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdAjuan()
    {
        return $this->hasOne(TaAjuan::className(), ['id_ajuan' => 'id_ajuan']);
    }
}
