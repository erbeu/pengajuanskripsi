<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "view_ta_ajuan".
 *
 * @property integer $id_ajuan
 * @property integer $id_mahasiswa
 * @property string $npm
 * @property string $nama_mahasiswa
 * @property string $kelas
 * @property integer $semester
 * @property string $tahun_masuk
 * @property string $prodi
 * @property string $judul_ajuan
 * @property string $tgl_ajuan
 * @property string $hasil_verifikasi
 * @property string $hasil_ujian
 */
class ViewTaAjuan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'view_ta_ajuan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_ajuan', 'id_mahasiswa', 'semester'], 'integer'],
            [['tahun_masuk', 'tgl_ajuan'], 'safe'],
            [['judul_ajuan', 'tgl_ajuan', 'hasil_verifikasi', 'hasil_ujian'], 'required'],
            [['hasil_verifikasi', 'hasil_ujian'], 'string'],
            [['npm', 'kelas'], 'string', 'max' => 10],
            [['nama_mahasiswa', 'prodi'], 'string', 'max' => 50],
            [['judul_ajuan'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_ajuan' => 'Id Ajuan',
            'id_mahasiswa' => 'Id Mahasiswa',
            'npm' => 'Npm',
            'nama_mahasiswa' => 'Nama Mahasiswa',
            'kelas' => 'Kelas',
            'semester' => 'Semester',
            'tahun_masuk' => 'Tahun Masuk',
            'prodi' => 'Prodi',
            'judul_ajuan' => 'Judul Ajuan',
            'tgl_ajuan' => 'Tgl Ajuan',
            'hasil_verifikasi' => 'Hasil Verifikasi',
            'hasil_ujian' => 'Hasil Ujian',
        ];
    }
}
