<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ta_ajuan".
 *
 * @property integer $id_ajuan
 * @property integer $id_mahasiswa
 * @property string $judul_ajuan
 * @property string $tgl_ajuan
 * @property string $hasil_verifikasi 
 * @property string $hasil_ujian 
 *
 * @property JudulFinal[] $judulFinals
 * @property RincianUjian[] $rincianUjians
 * @property UserMahasiswa $idMahasiswa
 * @property TaKandidat[] $taKandidats
 * @property TaUjian[] $taUjian
 * @property TaVerifikasi[] $taVerifikasi
 */
class TaAjuan extends \yii\db\ActiveRecord
{
    public $npm;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ta_ajuan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_mahasiswa', 'judul_ajuan', 'tgl_ajuan', 'hasil_verifikasi', 'hasil_ujian'], 'required'],
            [['id_mahasiswa', 'submit'], 'integer'],
            [['npm', 'tgl_ajuan'], 'safe'],
            [['hasil_verifikasi', 'hasil_ujian'], 'string'],
            [['judul_ajuan'], 'string', 'max' => 255],
            [['judul_ajuan'], 'unique'],
            [['id_mahasiswa'], 'exist', 'skipOnError' => true, 'targetClass' => UserMahasiswa::className(), 'targetAttribute' => ['id_mahasiswa' => 'id_mahasiswa']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_ajuan' => 'Id Ajuan',
            'id_mahasiswa' => 'Nama Mahasiswa',
            'judul_ajuan' => 'Judul Ajuan',
            'tgl_ajuan' => 'Tgl Ajuan',
            'hasil_verifikasi' => 'Hasil Verifikasi', 
            'hasil_ujian' => 'Hasil Ujian',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJudulFinals()
    {
        return $this->hasMany(JudulFinal::className(), ['id_ajuan' => 'id_ajuan']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRincianUjians()
    {
        return $this->hasMany(RincianUjian::className(), ['id_ajuan' => 'id_ajuan']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdMahasiswa()
    {
        return $this->hasOne(UserMahasiswa::className(), ['id_mahasiswa' => 'id_mahasiswa']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaVerifikasi()
    {
        return $this->hasOne(TaVerifikasi::className(), ['id_ajuan' => 'id_ajuan']);
    }
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getTaUjian()
    {
       return $this->hasOne(TaUjian::className(), ['id_ajuan' => 'id_ajuan']);
    }
}
