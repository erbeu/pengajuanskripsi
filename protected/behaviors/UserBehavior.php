<?php
namespace app\behaviors;

use Yii;

/**
 * @property yiiwebUser $owner
 */
class UserBehavior extends yii\base\Behavior
{
    private $_profiles;
 
    private function ensureLoaded()
    {
        if ($this->_profiles === null) {
            if (!$this->owner->isGuest) {
                $this->_profiles = (new yii\db\Query())
                    ->from('user_'.$this->owner->identity->role)
                    ->where(['id_user' => $this->owner->identity->id_user])
                    ->one();
            } else {
                $this->_profiles = false;
            }
        }
    }
 
    public function canGetProperty($name, $checkVars = true)
    {
        $this->ensureLoaded();
        return $this->_profiles && array_key_exists($name, $this->_profiles);
    }
 
    public function __get($name)
    {
        $this->ensureLoaded();
        return $this->_profiles && array_key_exists($name, $this->_profiles) ? $this->_profiles[$name] : null;
    }
}