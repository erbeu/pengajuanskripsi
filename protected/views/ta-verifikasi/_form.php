<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\TaVerifikasi */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ta-verifikasi-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?php
    if($model->isNewRecord || $model->id_admin == Yii::$app->user->id_admin) {
        array_shift(Yii::$app->params['hasil_verifikasi']);
        $hasil_verifikasi = array_combine(Yii::$app->params['hasil_verifikasi'], Yii::$app->params['hasil_verifikasi']);
        echo $form->field($modelTaAjuan, 'hasil_verifikasi')->dropDownList($hasil_verifikasi, ['prompt' => '']);
    }
    ?>

    <?= $form->field($model, 'catatan_verifikasi')->textarea(['rows' => 6]) ?>
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Buat' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-warning btn-flat' : 'btn btn-primary btn-flat']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
