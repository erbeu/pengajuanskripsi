<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TaVerifikasi */

$this->title = 'Detail';
$this->params['breadcrumbs'][] = ['label' => 'Skripsi Ajuan', 'url' => ['ta-ajuan/index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_ajuan, 'url' => ['ta-ajuan/view', 'id' => $model->id_ajuan]];
$this->params['breadcrumbs'][] = ['label' => 'Verifikasi', 'url' => ['index', 'id_ajuan' => $model->id_ajuan]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-verifikasi-view">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Detail Skripsi Verifikasi</h3>
                </div>
                <div class="box-body">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            //'id_verifikasi',
                            [
                                'attribute' => 'npm',
                                'value' => $model->idAjuan->idMahasiswa->npm,
                            ],
                            [
                                'attribute' => 'nama_mahasiswa',
                                'value' => $model->idAjuan->idMahasiswa->nama_mahasiswa,
                            ],
                            [
                                'attribute' => 'judul_ajuan',
                                'value' => $model->idAjuan->judul_ajuan,
                            ],
                            [
                                'attribute' => 'nama_admin',
                                'value' => $model->idAdmin->nama_admin,
                            ],
                            'tgl_verifikasi',
                            'catatan_verifikasi:ntext',
                        ],
                    ]) ?>
                </div>
                <div class="box-footer clearfix">
                    <?php
                    echo Html::a('Kembali', ['index', 'id_ajuan' => $model->id_ajuan], ['class' => 'btn btn-default btn-flat']);
                    /*echo Html::a('Hapus', ['delete', 'id' => $model->id_verifikasi], [
                            'class' => 'btn btn-danger btn-flat',
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                            ],
                        ]);*/
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>