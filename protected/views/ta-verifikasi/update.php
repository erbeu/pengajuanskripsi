<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TaVerifikasi */

$this->title = 'Update Verifikasi';
$this->params['breadcrumbs'][] = ['label' => 'Skripsi Ajuan', 'url' => ['ta-ajuan/index', 'id_mahasiswa' => $modelTaAjuan['id_mahasiswa']]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ta-verifikasi-update">
    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Detail Skripsi Ajuan</h3>
                </div>
                <div class="box-body">
                    <?= DetailView::widget([
                        'model' => $modelTaAjuan,
                        'attributes' => [
                            //'id_ajuan',
                            [
                                'attribute' => 'npm',
                                'value' => $modelTaAjuan->idMahasiswa->npm,
                            ],
                            [
                                'attribute' => 'mahasiswa',
                                'value' => $modelTaAjuan->idMahasiswa->nama_mahasiswa,
                            ],
                            'judul_ajuan',
                            'tgl_ajuan',
                        ],
                    ]) ?>
                </div>
                <div class="box-footer clearfix">
                    <?= Html::a('Kembali', ['ta-ajuan/index', 'id_mahasiswa' => $modelTaAjuan['id_mahasiswa']], ['class' => 'btn btn-primary btn-flat']) ?>
                </div>
            </div>
        </div>
        
        <div class="col-md-6">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Update Verifikasi</h3>
                </div>
                <div class="box-body">
                    <?= $this->render('_form', [
                        'model' => $model,
                        'modelTaAjuan' => $modelTaAjuan,
                        'modelAdmin' => $modelAdmin,
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>