<?php

use app\models\User;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TaVerifikasiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Skripsi Verifikasi';
$this->params['breadcrumbs'][] = ['label' => 'Skripsi Verifikasi', 'url' => ['ta-verifikasi/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-verifikasi-index">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Skripsi Verifikasi</h3>
                </div>
                <div class="box-body table-responsive">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'layout' => '{items}{summary}{pager}',
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            //'id_verifikasi',
                            [
                                'attribute' => 'npm',
                                'value' => 'idAjuan.idMahasiswa.npm'
                            ],
                            [
                                'attribute' => 'nama_mahasiswa',
                                'value' => 'idAjuan.idMahasiswa.nama_mahasiswa'
                            ],
                            [
                                'attribute' => 'judul_ajuan',
                                'value' => 'idAjuan.judul_ajuan',
                                'options' => ['width' => '30%'],
                            ],
                            [
                                'attribute' => 'nama_admin',
                                'value' => 'idAdmin.nama_admin'
                            ],
                            'tgl_verifikasi',
                            [
                                'attribute' => 'hasil_verifikasi',
                                'format' => 'html',
                                'value' => function($model) {
                                    $class = $model->idAjuan->hasil_verifikasi == 'diterima' ? 'success' : 'danger';
                                    return '<span class="label label-'.$class.'">'.$model->hasil_verifikasi.'</span>';
                                }
                            ],
                            /*[
                                'attribute' => 'catatan_verifikasi',
                                'format' => 'ntext',
                                'options' => ['width' => '30%'],
                            ],*/

                            [
                                'class' => 'yii\grid\ActionColumn',
                                'visibleButtons' => [
                                    'update' => false,
                                    'delete' => false,
                                ],
                            ],
                        ],
                    ]); ?>
                </div>
                <div class="box-footer clearfix">
                    
                </div>
            </div>
        </div>
    </div>
</div>