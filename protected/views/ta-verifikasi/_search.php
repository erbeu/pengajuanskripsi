<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TaVerifikasiSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ta-verifikasi-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_verifikasi') ?>

    <?= $form->field($model, 'id_admin') ?>

    <?= $form->field($model, 'id_ajuan') ?>

    <?= $form->field($model, 'tgl_verifikasi') ?>

    <?= $form->field($model, 'hasil_verifikasi') ?>

    <?php // echo $form->field($model, 'catatan_verifikasi') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
