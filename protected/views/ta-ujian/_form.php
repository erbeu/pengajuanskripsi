<?php

use app\models\TaAjuan;
use app\models\TaUjian;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RincianUjian */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ta-ujian-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?php
    $q = TaUjian::find()->where(['id_ajuan' => $modelTaAjuan->id_ajuan])->count();
    
    if(isset($model->catatan_penguji) || $q == 0) {
        if($model->pemberi_hasil == 1 && $model->submit == 0) {
            array_shift(Yii::$app->params['hasil_ujian']);
            $hasil_ujian = array_combine(Yii::$app->params['hasil_ujian'], Yii::$app->params['hasil_ujian']);
            echo $form->field($modelTaAjuan, 'hasil_ujian')->dropDownList($hasil_ujian);
        } elseif($q == 0) {
            array_shift(Yii::$app->params['hasil_ujian']);
            $hasil_ujian = array_combine(Yii::$app->params['hasil_ujian'], Yii::$app->params['hasil_ujian']);
            echo $form->field($modelTaAjuan, 'hasil_ujian')->dropDownList($hasil_ujian);
        }
    }
    ?>
    
    <?= $form->field($model, 'catatan_penguji')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Buat' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-warning btn-flat' : 'btn btn-primary btn-flat']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
