<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\RincianUjian */

$this->title = 'Buat Ujian';
$this->params['breadcrumbs'][] = ['label' => 'TA Ujian', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-ujian-create">
    <div class="row">
        <div class="col-md-4">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Detail Skripsi Ajuan</h3>
                </div>
                <div class="box-body">
                    <?= DetailView::widget([
                        'model' => $modelTaAjuan,
                        'attributes' => [
                            //'id_ajuan',
                            [
                                'attribute' => 'Npm',
                                'value' => $modelTaAjuan->idMahasiswa->npm,
                            ],
                            [
                                'attribute' => 'Mahasiswa',
                                'value' => $modelTaAjuan->idMahasiswa->nama_mahasiswa,
                            ],
                            'judul_ajuan',
                            'tgl_ajuan',
                            'hasil_verifikasi',
                            'hasil_ujian'
                        ],
                    ]) ?>
                </div>
                <div class="box-footer clearfix">
                    <?= Html::a('Kembali', ['index', 'id_ajuan' => $modelTaAjuan->id_ajuan], ['class' => 'btn btn-primary btn-flat']) ?>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Buat Ujian</h3>
                </div>
                <div class="box-body">
                    <?= $this->render('_form', [
                        'model' => $model,
                        'modelTaAjuan' => $modelTaAjuan,
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>