<?php

use app\models\User;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\DetailView;
use app\models\TaUjian;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RincianUjianSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Skripsi Ujian';
$this->params['breadcrumbs'][] = $this->title;

$user = Yii::$app->user;
?>
<div class="ta-ujian-index">
    <div class="row">
        <div class="col-md-4">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Detail Skripsi Ajuan</h3>
                </div>
                <div class="box-body">
                    <?= DetailView::widget([
                        'model' => $modelTaAjuan,
                        'attributes' => [
                            //'id_ajuan',
                            [
                                'attribute' => 'Npm',
                                'value' => $modelTaAjuan->idMahasiswa->npm,
                            ],
                            [
                                'attribute' => 'Mahasiswa',
                                'value' => $modelTaAjuan->idMahasiswa->nama_mahasiswa,
                            ],
                            'judul_ajuan',
                            'tgl_ajuan',
                            'hasil_verifikasi',
                            'hasil_ujian'
                        ],
                    ]) ?>
                </div>
                <div class="box-footer clearfix">
                    <?= Html::a('Kembali', ['ta-ajuan/index', 'id_mahasiswa' => $modelTaAjuan->id_mahasiswa], ['class' => 'btn btn-primary btn-flat']) ?>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Daftar Skripsi Ujian</h3>
                </div>
                <div class="box-body table-responsive">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        //'filterModel' => $searchModel,
                        'layout' => '{items}{summary}{pager}',
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            //'id_ujian',
                            'detailUjian.idPenguji.nama_penguji',
                            'catatan_penguji:ntext',

                            [
                                'class' => 'yii\grid\ActionColumn',
                                'visibleButtons' => [
                                    'view' => false,
                                    'update' => function($model) use($user) {
                                        return $user->id_penguji == $model->detailUjian->id_penguji ? true :  false;
                                    },
                                    'delete' => false,
                                ]
                            ],
                        ],
                    ]); ?>
                </div>
                <div class="box-footer clearfix">
                    <?php
                    $condition = TaUjian::find()
                        ->join('LEFT JOIN', 'detail_ujian', 'detail_ujian.id_detail = ta_ujian.id_detail')
                        ->where(['id_penguji' => $modelPenguji['id_penguji'], 'id_ajuan' => $modelTaAjuan->id_ajuan])
                        ->exists();
                    if(!$condition) {
                        echo Html::a('Buat Ujian', ['create', 'id_ajuan' => $modelTaAjuan->id_ajuan], ['class' => 'btn btn-warning btn-flat']);
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>