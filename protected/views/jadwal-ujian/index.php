<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserPengujiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Jadwal Ujian Judul Skripsi';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jadwal-ujian-index">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Daftar Penguji & Jadwal Ujian</h3>
                </div>
                <div class="box-body table-responsive">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'layout' => '{items}{summary}{pager}',
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            //'id_penguji',
                            //'id_user',
                            'nik',
                            'nama_penguji',
                            'jafung',

                            [
                                'class' => 'yii\grid\ActionColumn',
                                'buttons' => [
                                    'view' => function ($url, $model, $key) {
                                        return Html::a('Detail Jadwal Ujian', $url, ['class' => 'btn btn-success btn-flat btn-sm']);
                                    },
                                ],
                                'visibleButtons' => [
                                    'update' => false,
                                    'delete' => false,
                                ]
                            ],
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</div>