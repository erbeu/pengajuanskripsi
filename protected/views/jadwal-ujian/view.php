<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserPengujiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Jadwal Ujian';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jadwal-ujian-index">
    <div class="row">
        <div class="col-md-4">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Detail Penguji</h3>
                </div>
                <div class="box-body">
                    <?= DetailView::widget([
                        'model' => $modelPenguji,
                        'attributes' => [
                            //'id_ajuan',
                            'nik',
                            'nama_penguji',
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
        
        <div class="col-md-8">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Daftar Jadwal Ujian</h3>
                </div>
                <div class="box-body table-responsive">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'layout' => '{items}{summary}{pager}',
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            //'id_penguji',
                            //'id_user',
                            'npm',
                            'nama_mahasiswa',
                            [
                                'attribute' => 'tgl_ujian',
                                'value' => function($model) {
                                    return ($model->detailUjian[0]->tgl_ujian);
                                }
                            ],
                            [
                                'attribute' => 'ruang',
                                'value' => function($model) {
                                    return ($model->detailUjian[0]->ruang);
                                }
                            ]
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</div>