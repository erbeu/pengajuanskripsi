<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\HalamanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Upload Media';
$this->params['breadcrumbs'][] = ['label' => 'Media', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="upload-media-index">
    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Upload Media</h3>
                </div>
                <div class="box-body">
                    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

                        <?= $form->field($model, 'imageFile')->fileInput()->label('Pilih Media') ?>

                        <?= Html::submitButton('Upload', ['class' => 'btn btn-warning btn-flat']) ?>

                    <?php ActiveForm::end() ?>
                </div>
            </div>
        </div>
    </div>
</div>