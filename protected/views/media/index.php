<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use app\widgets\Alert;

/* @var $this yii\web\View */
/* @var $searchModel app\models\HalamanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Media';
$this->params['breadcrumbs'][] = $this->title;
$this->registerJs('
    var clipboard = new Clipboard(".btn-copy");
    clipboard.on("success", function(e) {
        alert("Berhasil disalin.");
        console.info("Action:", e.action);
        console.info("Text:", e.text);
        console.info("Trigger:", e.trigger);

        e.clearSelection();
    });
    $(".img-container").gridalicious({selector: ".img"});
');
?>
<div class="media-index">
    <div class="row">
        <div class="col-md-12">
            <?= Alert::widget() ?>
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Daftar Media</h3>
                </div>
                <div class="box-body">
                    <div class="img-container">
                        <?php
                        $i = 0;
                        foreach($data['files'] as $file) {
                            $filename = explode('\\', $file)[1];
                            $path = Url::base(true).'/uploads/'.$filename;
                            ?>
                            <div class="img">
                                <?= Html::img($path) ?>
                                <br><br>
                                <div class="input-group">
                                    <?= Html::textInput('path', $path, ['readonly' => true, 'class' => 'form-control', 'id' => 'a'.$i]) ?>
                                    <span class="input-group-btn">
                                        <?= Html::button('<i class="glyphicon glyphicon-share"></i>', [
                                            'class' => 'btn btn-success btn-flat btn-copy',
                                            'title' => 'Salin URL',
                                            'data' => [
                                                'toggle' => 'tooltip',
                                                'placement' => 'top',
                                                'clipboard-target' => '#a'.$i
                                            ]
                                        ]) ?>
                                    </span>
                                    <span class="input-group-btn">
                                        <?= Html::a('<i class="glyphicon glyphicon-trash"></i>', ['delete', 'filename' => $filename], [
                                            'class' => 'btn btn-danger btn-flat',
                                            'title' => 'Hapus',
                                            'data' => [
                                                'toggle' => 'tooltip',
                                                'placement' => 'top',
                                                'confirm' => 'Are you sure you want to delete this item?',
                                                'method' => 'post',
                                            ],
                                        ]) ?>
                                    </span>
                                </div>
                                <br><br>
                            </div>
                            <?php
                            $i++;
                        }
                        ?>
                    </div>
                </div>
                <div class="box-footer clearfix">
                    <?= Html::a('Upload Media', ['create'], ['class' => 'btn btn-warning btn-flat']) ?>
                </div>
            </div>
        </div>
    </div>
</div>