<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Pengumuman */

$this->title = $model->judul;
$this->params['breadcrumbs'][] = ['label' => 'Pengumuman', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengumuman-view">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Detail Pengumuman</h3>
                </div>
                <div class="box-body">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'id_pengumuman',
                            'judul',
                            [
                                'attribute' => 'isi',
                                'format' => 'html',
                            ],
                            [
                                'attribute' => 'created_by',
                                'value' => $model->createdBy->nama_admin,
                            ],
                            'created_at',
                            [
                                'attribute' => 'updated_by',
                                'value' => $model->updatedBy->nama_admin,
                            ],
                            'updated_at',
                        ],
                    ]) ?>
                </div>
                <div class="box-footer clearfix">
                    <?= Html::a('Update', ['update', 'id' => $model->id_pengumuman], ['class' => 'btn btn-primary btn-flat']) ?>
                    <?= Html::a('Hapus', ['delete', 'id' => $model->id_pengumuman], [
                        'class' => 'btn btn-danger btn-flat',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>