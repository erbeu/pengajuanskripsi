<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model app\models\Pengumuman */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="callout callout-info">
    <h4>Informasi</h4>
    <p>Untuk memberi gambar pada pengumuman, anda dapat menggunakan menu <?= Html::a('<kbd>Media</kbd>', ['media/index'], ['target' => '_blank']) ?> untuk mengunggah gambar, lalu salin url gambar dan dalam textarea isi halaman pilih icon gambar kemudian paste url gambar yang sudah disalin sebelumnya pada kolom URL.</p>
</div>
<div class="pengumuman-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'judul')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'isi')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'basic'
    ]) ?>
    
    <?= !$model->isNewRecord ? $form->field($model, 'arsip')->dropDownList([0 => 'Aktif', 1 => 'Arsip'], ['prompt' => '']) : '' ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Buat' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-warning btn-flat' : 'btn btn-primary btn-flat']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>