<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PengumumanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pengumuman';
$this->params['breadcrumbs'][] = $this->title;

if(Yii::$app->request->get('page') == '') {
    $btn_class[0] = 'primary';
    $btn_class[1] = 'default';
} elseif(Yii::$app->request->get('page') == 'arsip') {
    $btn_class[0] = 'default';
    $btn_class[1] = 'primary';
}
?>
<div class="pengumuman-index">
    <div class="row">
        <div class="col-md-2">
            <?= Html::a('Pengumuman Aktif', ['index'], ['class' => 'btn btn-block btn-flat btn-'.$btn_class[0]]) ?>
        </div>
        <div class="col-md-2">
            <?= Html::a('Arsip', ['index', 'page' => 'arsip'], ['class' => 'btn btn-block btn-flat btn-'.$btn_class[1]]) ?>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Daftar Pengumuman</h3>
                </div>
                <div class="box-body table-responsive">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'layout' => '{items}{summary}{pager}',
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            // 'id_pengumuman',
                            'judul',
                            // 'isi:ntext',
                            [
                                'attribute' => 'created_by',
                                'value' => 'createdBy.nama_admin'
                            ],
                            'created_at',
                            // 'updated_by',
                            // 'updated_at',

                            ['class' => 'yii\grid\ActionColumn'],
                        ],
                    ]); ?>
                </div>
                <div class="box-footer clearfix">
                    <?= Html::a('Buat Pengumuman', ['create'], ['class' => 'btn btn-warning btn-flat']) ?>
                </div>
            </div>
        </div>
    </div>
</div>