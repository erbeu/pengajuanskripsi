<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\ViewTaAjuan;
use app\models\User;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserMahasiswaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Laporan Mahasiswa';
$this->params['breadcrumbs'][] = $this->title;

$gridColumns = [
    ['class' => 'yii\grid\SerialColumn'],

    // 'id_mahasiswa',
    // 'id_user',
    'npm',
    'nama_mahasiswa',
    'jenis_kelamin',
    'kelas',
    'tahun_masuk',
    'prodi',
    'jumlah_judul',
];
?>
<div class="user-mahasiswa-index">
    <div class="row">
        <div class="col-md-3 side-filter">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Filter</h3>
                </div>
                <div class="box-body table-responsive">
                    <?= $this->render('_search', ['model' => $searchModel, 'data' => $data]) ?>
                    <hr>
                    <?= ExportMenu::widget([
                        'dataProvider' => $dataProvider,
                        'columns' => $gridColumns,
                        'target' => '_blank',
                        'asDropdown' => false,
                        'filename' => $this->title,
                        'exportConfig' => [
                            'CSV' => false,
                            'HTML' => false,
                            'TXT' => false,
                            'PDF' => false,
                            'Excel5' => false,
                            'Excel2007' => [
                                'label' => 'Export Excel',
                                'options' => ['class' => 'btn btn-primary btn-flat btn-block']
                            ]
                        ]
                    ]) ?>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Daftar Mahasiswa</h3>
                </div>
                <div class="box-body table-responsive">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'layout' => '{items}{summary}{pager}',
                        'columns' => $gridColumns,
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>