<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UserMahasiswaSearch */
/* @var $form yii\widgets\ActiveForm */

$list_kelas = array_combine(Yii::$app->params['list_kelas'], Yii::$app->params['list_kelas']);
$list_prodi = array_combine(Yii::$app->params['list_prodi'], Yii::$app->params['list_prodi']);
$list_tahun = array_combine($data['list_tahun'], $data['list_tahun']);
?>

<div class="user-mahasiswa-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'kelas')->dropDownList($list_kelas, ['prompt' => '']) ?>

    <?= $form->field($model, 'tahun_masuk')->dropDownList($list_tahun, ['prompt' => '']) ?>

    <?= $form->field($model, 'prodi')->dropDownList($list_prodi, ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton('Cari', ['class' => 'btn btn-warning btn-flat']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default btn-flat']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
