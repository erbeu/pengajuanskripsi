<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UserMahasiswaSearch */
/* @var $form yii\widgets\ActiveForm */

$list_tahun = array_combine($data['list_tahun'], $data['list_tahun']);
?>

<div class="user-penguji-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'nik') ?>

    <?= $form->field($model, 'nama_penguji') ?>

    <?= $form->field($model, 'tahun')->dropDownList($list_tahun, ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton('Cari', ['class' => 'btn btn-warning btn-flat']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default btn-flat']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
