<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Halaman */

$this->title = 'Update Halaman: ' . $model->id_halaman;
$this->params['breadcrumbs'][] = ['label' => 'Halaman', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_halaman, 'url' => ['view', 'id' => $model->id_halaman]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="halaman-update">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Update Pengumuman</h3>
                </div>
                <div class="box-body">
                    <?= $this->render('_form', [
                        'model' => $model,
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>