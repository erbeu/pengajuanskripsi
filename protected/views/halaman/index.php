<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\HalamanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Halaman';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="halaman-index">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Daftar Halaman</h3>
                </div>
                <div class="box-body table-responsive">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'layout' => '{items}{summary}{pager}',
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            //'id_halaman',
                            'judul_halaman',
                            //'isi_halaman:ntext',
                            [
                                'attribute' => 'created_by',
                                'value' => 'createdBy.nama_admin'
                            ],
                            'created_at',
                            // 'updated_by',
                            // 'updated_at',

                            ['class' => 'yii\grid\ActionColumn'],
                        ],
                    ]); ?>
                </div>
                <div class="box-footer clearfix">
                    <?= Html::a('Buat Halaman', ['create'], ['class' => 'btn btn-warning btn-flat']) ?>
                </div>
            </div>
        </div>
    </div>
</div>