<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="site-error">
    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <div class="box-body">
                    <div class="alert alert-danger">
                        <i class="icon fa fa-ban"></i> <?= nl2br(Html::encode($message)) ?>
                    </div>

                    <p>
                        The above error occurred while the Web server was processing your request.
                    </p>
                    <p>
                        Please contact us if you think this is a server error. Thank you.
                    </p>
                </div>
                <div class="box-footer clearfix">
                    <?= Html::a('Kembali ke halaman utama', ['site/index'], ['class' => 'btn btn-primary btn-flat']) ?>
                </div>
            </div>
        </div>
    </div>
</div>