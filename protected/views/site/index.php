<?php

use yii\helpers\Html;
use yii\helpers\Url;
use app\widgets\Alert;
use yii\widgets\LinkPager;
/* @var $this yii\web\View */

$this->title = 'Selamat Datang';
?>

<div class="row bg-blue">
    <div class="col-md-12">
        <h2 class="sub"><i class="glyphicon glyphicon-home"></i> Selamat Datang !</h2>

        <p><img src="<?= Url::base() ?>/assets-static/img/graduation.png" class="img-responsive pull-right" width="100px" alt="Image">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis praesentium optio quasi alias, aliquam eligendi, repellat non. A quam eveniet illum optio similique, atque sunt. Neque molestias impedit eligendi culpa veniam illum iusto explicabo libero veritatis quo doloribus, voluptatem quidem ipsam pariatur ad eaque ratione accusantium aut. Amet, harum, ratione.</p>

        <p><?= Html::a('<i class="glyphicon glyphicon-user"></i> Login', ['login'], ['class' => 'btn bg-yellow btn-lg']) ?></p>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <h3 class="sub"><i class="glyphicon glyphicon-list"></i> Seputar Skripsi Mahasiswa</h3>
        <div class="btn-group-vertical btn-block">
            <?php foreach($data['halaman'] as $post) { ?>
                <?= Html::a('<i class="glyphicon glyphicon-search"></i> '.$post['judul_halaman'], ['halaman', 'id' => $post['id_halaman']], ['class' => 'btn btn-blue']) ?>
            <?php } ?>
        </div>
        <?= LinkPager::widget([
            'pagination' => $pages['halaman'],
        ]) ?>
    </div>
    <div class="col-md-6">
        <h3 class="sub"><i class="glyphicon glyphicon-list"></i> Pengumuman</h3>
        
        <?php foreach($data['pengumuman'] as $post) { ?>
            <div class="post">
                <b><?= $post['judul'] ?></b> - <?= substr(strip_tags($post['isi']), 0, 200) ?> ... <?= Html::a('Selengkapnya', ['pengumuman', 'id' => $post['id_pengumuman']]) ?>
            </div>
        <?php } ?>
        <?= LinkPager::widget([
            'pagination' => $pages['pengumuman'],
        ]) ?>
    </div>
</div>