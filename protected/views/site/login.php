<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>

<p class="login-box-msg">Sign in to start your session</p>
<?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
   
    <?= $form->field($model, 'username', [
        'template' => '{label}{input}<span class="glyphicon glyphicon-user form-control-feedback"></span>{hint}{error}',
        'options' => ['class' => 'has-feedback']
    ])->textInput(['placeholder'=>'Username']) ?>

    <?= $form->field($model, 'password', [
        'template' => '{label}{input}<span class="glyphicon glyphicon-lock form-control-feedback"></span>{hint}{error}',
        'options' => [ 'class' => 'has-feedback']
    ])->passwordInput(['placeholder'=>'Password']) ?>
    
    <div class="row">
        <div class="col-xs-8">
            <?php /* $form->field($model, 'rememberMe', [
                'checkboxTemplate' => '{input} {label}',
                'options' => ['class' => 'checkbox icheck']
            ])->checkbox()*/ ?>
        </div><!-- /.col -->
        <div class="col-xs-4">
            <?= Html::submitButton('Login <span class=\'fa fa-arrow-circle-o-right\'></span>', ['class' => 'btn btn-primary btn-block btn-flat', 'name' => 'login-button']) ?>
        </div><!-- /.col -->
    </div>
<?php ActiveForm::end(); ?>

<hr>
<p class="text-center">&copy; <?= date('Y') ?></p>