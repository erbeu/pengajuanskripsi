<?php

use yii\helpers\Html;
use yii\helpers\Url;
use app\widgets\Alert;
use yii\widgets\LinkPager;
/* @var $this yii\web\View */

$this->title = $data['halaman']['judul_halaman'];
?>

<div class="row">
    <div class="col-md-12">
        <h2 class="sub"><i class="glyphicon glyphicon-list"></i> <?= $data['halaman']['judul_halaman'] ?></h2>

        <?= $data['halaman']['isi_halaman'] ?>
        <div class="clearfix"></div>
        <p><?= Html::a('<i class="glyphicon glyphicon-chevron-left"></i> Kembali', ['index'], ['class' => 'btn bg-blue btn-lg']) ?></p>
    </div>
</div>