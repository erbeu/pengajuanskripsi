<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Sign Up';
$this->params['breadcrumbs'][] = $this->title;
?>

<p class="login-box-msg">Register a new membership</p>
<?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
   
    <?= $form->field($model, 'username', [
        'template' => '{label}{input}<span class="glyphicon glyphicon-user form-control-feedback"></span>{hint}{error}',
        'options' => ['class' => 'has-feedback']
    ])->textInput(['placeholder'=>'Username']) ?>

    <?= $form->field($model, 'password', [
        'template' => '{label}{input}<span class="glyphicon glyphicon-lock form-control-feedback"></span>{hint}{error}',
        'options' => ['class' => 'has-feedback']
    ])->passwordInput(['placeholder'=>'Password']) ?>
    
    <div class="row">
        <div class="col-xs-8">
        </div><!-- /.col -->
        <div class="col-xs-4">
            <?= Html::submitButton('Signup <span class=\'fa fa-arrow-circle-o-right\'></span>', ['class' => 'btn btn-primary btn-block btn-flat', 'name' => 'signup-button']) ?>
        </div><!-- /.col -->
    </div>
<?php ActiveForm::end(); ?>

<hr>
<div class="row">
    <div class="col-md-6">
        <?= Html::a('<span class=\'fa fa-arrow-circle-o-left\'></span> Login', ['site/login'], ['class'=>'btn btn-success btn-block btn-flat']) ?>
    </div>
</div>