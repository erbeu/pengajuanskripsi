<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UserMahasiswa */

$list_kelas = array_combine(Yii::$app->params['list_kelas'], Yii::$app->params['list_kelas']);
$list_semester = array_combine(Yii::$app->params['list_semester'], Yii::$app->params['list_semester']);
$list_prodi = array_combine(Yii::$app->params['list_prodi'], Yii::$app->params['list_prodi']);
$list_tahun = array_combine($data['list_tahun'], $data['list_tahun']);
$list_kelamin = ['L' => 'Laki - Laki', 'P' => 'Perempuan'];

$this->title = 'Edit User';
$this->params['breadcrumbs'][] = ['label' => 'Dashboard', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="edit-user-mahasiswa">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <?php $form = ActiveForm::begin(); ?>

                    <?= $form->field($model, 'nama_mahasiswa')->textInput(['maxlength' => true]) ?>
    
                    <?= $form->field($model, 'jenis_kelamin')->dropDownList($list_kelamin, ['prompt' => '']) ?>

                    <?= $form->field($model, 'kelas')->dropDownList($list_kelas, ['prompt' => '']) ?>

                    <?= $form->field($model, 'semester')->dropDownList($list_semester, ['prompt' => '']) ?>

                    <?= $form->field($model, 'tahun_masuk')->dropDownList($list_tahun, ['prompt' => '']) ?>

                    <?= $form->field($model, 'prodi')->dropDownList($list_prodi, ['prompt' => '']) ?>

                    <?= $form->field($user, 'pass')->passwordInput().'<p class="help-block">Kosongkan password jika tidak ingin diganti.</p>' ?>

                    <div class="form-group">
                        <?= Html::submitButton('Simpan', ['class' => 'btn btn-primary btn-flat']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>