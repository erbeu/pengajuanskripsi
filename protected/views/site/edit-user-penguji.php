<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UserMahasiswa */

$list_kelamin = ['L' => 'Laki - Laki', 'P' => 'Perempuan'];

$this->title = 'Edit User';
$this->params['breadcrumbs'][] = ['label' => 'Dashboard', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="edit-user-mahasiswa">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <?php $form = ActiveForm::begin(); ?>

                    <?= $model->isNewRecord ? $form->field($model, 'nik')->textInput(['maxlength' => true]) : '' ?>

                    <?= $form->field($model, 'nama_penguji')->textInput(['maxlength' => true]) ?>
    
                    <?= $form->field($model, 'jenis_kelamin')->dropDownList($list_kelamin, ['prompt' => '']) ?>

                    <?= $form->field($model, 'jafung')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($user, 'pass')->passwordInput().'<p class="help-block">Kosongkan password jika tidak ingin diganti.</p>' ?>

                    <div class="form-group">
                        <?= Html::submitButton('Simpan', ['class' => 'btn btn-primary btn-flat']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>