<?php

use yii\helpers\Html;
use app\widgets\Alert;
/* @var $this yii\web\View */

$this->title = 'Dashboard';
?>

<div class="row">
    <div class="col-md-12">
        <div class="box box-solid bg-light-blue jumbo">
            <div class="box-body">
                <h3><?= Yii::$app->name ?></h3>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-4 col-xs-4">
        <!-- small box -->
        <div class="small-box bg-aqua">
            <div class="inner">
                <h3>1</h3>
                <p>Kategori Bakat</p>
            </div>
            <div class="icon">
                <i class="ion ion-bag"></i>
            </div>
            <?= Html::a('Selengkapnya <i class="fa fa-arrow-circle-right"></i>', ['bakat/index'], ['class' => 'small-box-footer']) ?>
        </div>
    </div><!-- ./col -->
    <div class="col-lg-4 col-xs-4">
        <!-- small box -->
        <div class="small-box bg-green">
            <div class="inner">
                <h3>2</h3>
                <p>Ciri Bakat</p>
            </div>
            <div class="icon">
                <i class="ion ion-stats-bars"></i>
            </div>
            <?= Html::a('Selengkapnya <i class="fa fa-arrow-circle-right"></i>', ['ciri/index'], ['class' => 'small-box-footer']) ?>
        </div>
    </div><!-- ./col -->
    <div class="col-lg-4 col-xs-4">
        <!-- small box -->
        <div class="small-box bg-red">
            <div class="inner">
                <h3>3</h3>
                <p>Pengguna</p>
            </div>
            <div class="icon">
                <i class="ion ion-pie-graph"></i>
            </div>
            <?= Html::a('Selengkapnya <i class="fa fa-arrow-circle-right"></i>', ['user/index'], ['class' => 'small-box-footer']) ?>
        </div>
    </div><!-- ./col -->
</div><!-- /.row -->

<?= Alert::widget() ?>
<div class="row">
    <div class="col-md-6">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Bordered Table</h3>
            </div>
            <div class="box-body">
                <table class="table table-bordered">
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Task</th>
                        <th>Progress</th>
                        <th style="width: 40px">Label</th>
                    </tr>
                    <tr>
                        <td>1.</td>
                        <td>Update software</td>
                        <td>
                            <div class="progress progress-xs">
                                <div class="progress-bar progress-bar-danger" style="width: 55%"></div>
                            </div>
                        </td>
                        <td><span class="badge bg-red">55%</span></td>
                    </tr>
                    <tr>
                        <td>2.</td>
                        <td>Clean database</td>
                        <td>
                            <div class="progress progress-xs">
                                <div class="progress-bar progress-bar-yellow" style="width: 70%"></div>
                            </div>
                        </td>
                        <td><span class="badge bg-yellow">70%</span></td>
                    </tr>
                    <tr>
                        <td>3.</td>
                        <td>Cron job running</td>
                        <td>
                            <div class="progress progress-xs progress-striped active">
                                <div class="progress-bar progress-bar-primary" style="width: 30%"></div>
                            </div>
                        </td>
                        <td><span class="badge bg-light-blue">30%</span></td>
                    </tr>
                    <tr>
                        <td>4.</td>
                        <td>Fix and squish bugs</td>
                        <td>
                            <div class="progress progress-xs progress-striped active">
                                <div class="progress-bar progress-bar-success" style="width: 90%"></div>
                            </div>
                        </td>
                        <td><span class="badge bg-green">90%</span></td>
                    </tr>
                </table>
            </div>
            <div class="box-footer clearfix">
                <ul class="pagination pagination-sm no-margin pull-right">
                    <li><a href="#">&laquo;</a></li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">&raquo;</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">About this application</h3>
            </div>
            <div class="box-body">
                <dl class="dl-horizontal">
                    <dt>App Name</dt>
                    <dd><?= Yii::$app->name ?></dd>
                    <dt>App Version</dt>
                    <dd>1.0</dd>
                    <dt>App Developer</dt>
                    <dd>Rizky Bintang Utama.</dd>
                    <dt>Dev Homepage</dt>
                    <dd><a href="http://rizkybintangutama.cu.cc">rizkybintangutama.cu.cc</a></dd>
                </dl>
            </div>
        </div>
    </div>
</div>