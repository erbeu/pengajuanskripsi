<?php

use yii\helpers\Html;
use yii\helpers\Url;
use app\widgets\Alert;
use yii\widgets\LinkPager;
/* @var $this yii\web\View */

$this->title = $data['pengumuman']['judul'];
?>

<div class="row">
    <div class="col-md-12">
        <h2 class="sub"><i class="glyphicon glyphicon-list"></i> <?= $data['pengumuman']['judul'] ?></h2>

        <?= $data['pengumuman']['isi'] ?>
        <div class="clearfix"></div>
        <p><?= Html::a('<i class="glyphicon glyphicon-chevron-left"></i> Kembali', ['index'], ['class' => 'btn bg-blue btn-lg']) ?></p>
    </div>
</div>