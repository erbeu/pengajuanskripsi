<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\TaAjuan;
use app\models\User;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserMahasiswaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Judul Skripsi Lolos Verifikasi';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-lolos-verifikasi-index">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Daftar Judul Skripsi Yang Sudah Lolos Verifikasi</h3>
                </div>
                <div class="box-body table-responsive">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'layout' => '{items}{summary}{pager}',
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            // 'id_mahasiswa',
                            // 'id_user',
                            'npm',
                            'nama_mahasiswa',
                            'kelas',
                            'semester',
                            'tahun_masuk',
                            'prodi',
                            [
                                'attribute' => 'menu',
                                'format' => 'html',
                                'options' => ['width' => '20%'],
                                'value' => function($model) {
                                    $return = Html::a('Skripsi Ajuan', ['ta-ajuan/index', 'id_mahasiswa' => $model->id_mahasiswa], ['class' => 'btn btn-success btn-sm btn-flat']);
                                    $query = TaAjuan::find()
                                        ->where([
                                            'id_mahasiswa' => $model->id_mahasiswa,
                                            'hasil_verifikasi' => Yii::$app->params['hasil_verifikasi'][1]
                                        ])
                                        ->count();
                                    if($query == 3 && Yii::$app->user->identity->role == User::ROLE_ADMIN) {
                                        $return .= Html::a('Buat Detail Ujian', ['detail-ujian/index', 'id_mahasiswa' => $model->id_mahasiswa], ['class' => 'btn btn-success btn-sm btn-flat']);
                                    }
                                    return $return;
                                }
                            ]
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</div>