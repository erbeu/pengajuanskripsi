<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\JudulFinal */

$this->title = 'Create Judul Final';
$this->params['breadcrumbs'][] = ['label' => 'Judul Finals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="judul-final-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
