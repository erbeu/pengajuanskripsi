<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\JudulFinal */

$this->title = 'Update Judul Final: ' . $model->id_judul;
$this->params['breadcrumbs'][] = ['label' => 'Judul Finals', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_judul, 'url' => ['view', 'id' => $model->id_judul]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="judul-final-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
