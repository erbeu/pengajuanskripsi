<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\JudulFinalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Judul Skripsi Final';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="judul-final-index">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Daftar Judul Skripsi Final</h3>
                </div>
                <div class="box-body table-responsive">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'layout' => '{items}{summary}{pager}',
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            
                           //'id_judul',
                           //'id_ajuan',
                           'npm',
                           'nama_mahasiswa',
                           'jenis_kelamin',
                           'kelas',
                           'tahun_masuk',
                           'prodi',
                           'judul',
                           'tahun_ajuan',

                            //['class' => 'yii\grid\ActionColumn'],
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</div>