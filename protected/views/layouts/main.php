<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\AdminLTEAsset;
use app\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;

AdminLTEAsset::register($this);
$this->registerJs('
    $(".sidebar-menu").removeClass("nav");
    $(".sidebar-menu li").removeClass("dropdown");
    $(".sidebar-menu li a").removeClass("dropdown-toggle");
    $(".sidebar-menu li ul").removeClass("dropdown-menu");
');

$nama = '';
if(Yii::$app->user->identity->role == User::ROLE_ADMIN) {
    $nama = Yii::$app->user->nama_admin;
} elseif(Yii::$app->user->identity->role == User::ROLE_PENGUJI) {
    $nama = Yii::$app->user->nama_penguji;
} elseif(Yii::$app->user->identity->role == User::ROLE_MAHASISWA) {
    $nama = Yii::$app->user->nama_mahasiswa;
}
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?> | <?= Yii::$app->name ?></title>
    <?php $this->head() ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body class="skin-blue">
    <?php $this->beginBody() ?>
    <div class="wrapper">

        <!-- Main Header -->
        <header class="main-header">

            <!-- Logo -->
            <?= Html::a(Yii::$app->name, ['site/index'], ['class'=>'logo']) ?>

            <!-- Header Navbar -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>
                <!-- Navbar Right Menu -->
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <!-- User Account Menu -->
                        <li class="dropdown user user-menu">
                            <!-- Menu Toggle Button -->
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <!-- The user image in the navbar-->
                                <img src="<?= Url::base() ?>/assets-static/img/avatar-<?= Yii::$app->user->jenis_kelamin ?>.png" class="user-image" alt="Avatar"/>
                                <!-- hidden-xs hides the username on small devices so only the image appears. -->
                                <span class="hidden-xs"><?= $nama ?></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- The user image in the menu -->
                                <li class="user-header">
                                    <img src="<?= Url::base() ?>/assets-static/img/avatar-<?= Yii::$app->user->jenis_kelamin ?>.png" class="img-circle" alt="Avatar" />
                                    <p>
                                        <?= $nama ?>
                                        <small><?= Yii::$app->user->identity->role ?></small>
                                    </p>
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <?= Html::a('Edit User', ['site/edit-user'], ['class' => 'btn btn-warning btn-flat']) ?>
                                    </div>
                                    <div class="pull-right">
                                        <?= Html::a('Sign Out', ['site/logout'], ['data-method' => 'post', 'class' => 'btn btn-danger btn-flat']) ?>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="main-sidebar">

            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
                <?php if (!Yii::$app->user->isGuest) : ?>
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="<?= Url::base() ?>/assets-static/img/avatar-<?= Yii::$app->user->jenis_kelamin ?>.png" class="img-circle" alt="Avatar"/>
                        </div>
                        <div class="pull-left info">
                            <p>Hello, <?= $nama ?></p>
                            <a href="javascript:void(0);">
                                <i class="fa fa-circle text-success"></i> <?= Yii::$app->user->identity->role ?>
                            </a>
                        </div>
                    </div>
                <?php endif ?>

                <ul class="sidebar-menu">
                    <li class="header">MAIN NAVIGATION</li>
                </ul>
                <?php
                    $items[] = [
                        'label' => '<i class="fa fa-list"></i> Media',
                        'url' => ['media/index'],
                        'active' => $this->context->id == 'media',
                        'visible' => Yii::$app->user->identity->role == User::ROLE_ADMIN
                    ];
                    $items[] = [
                        'label' => '<i class="fa fa-list"></i> Halaman',
                        'url' => ['halaman/index'],
                        'active' => $this->context->id == 'halaman',
                        'visible' => Yii::$app->user->identity->role == User::ROLE_ADMIN
                    ];
                    $items[] = [
                        'label' => '<i class="fa fa-list"></i> Pengumuman',
                        'url' => ['pengumuman/index'],
                        'active' => $this->context->id == 'pengumuman',
                        'visible' => Yii::$app->user->identity->role == User::ROLE_ADMIN
                    ];
                    $items[] = [
                        'label' => '<i class="fa fa-list"></i> Skripsi Ajuan',
                        'url' => ['ta-ajuan/index', 'id_mahasiswa' => Yii::$app->user->identity->userMahasiswa['id_mahasiswa']],
                        'active' => $this->context->id == 'ta-ajuan',
                        'visible' => Yii::$app->user->identity->role == User::ROLE_MAHASISWA
                    ];
                    $items[] = [
                        'label' => '<i class="fa fa-list"></i> Cari Judul',
                        'url' => ['cari-judul/index'],
                        'active' => $this->context->id == 'cari-judul',
                        'visible' => in_array(Yii::$app->user->identity->role, [User::ROLE_ADMIN, User::ROLE_MAHASISWA])
                    ];
                    $items[] = [
                        'label' => '<i class="fa fa-list"></i> Judul Skripsi Ajuan',
                        'url' => ['ta-mahasiswa/index'],
                        'active' => in_array($this->context->id, ['ta-mahasiswa']),
                        'visible' => Yii::$app->user->identity->role == User::ROLE_ADMIN || Yii::$app->user->identity->role == User::ROLE_PENGUJI
                    ];
                    $items[] = [
                        'label' => '<i class="fa fa-list"></i> Judul Skripsi Lolos Verifikasi',
                        'url' => ['ta-lolos-verifikasi/index'],
                        'active' => in_array($this->context->id, ['ta-lolos-verifikasi']),
                        'visible' => Yii::$app->user->identity->role == User::ROLE_ADMIN
                    ];
                    $items[] = [
                        'label' => '<i class="fa fa-list"></i> Judul Skripsi Pra Uji',
                        'url' => ['ta-pra-uji/index'],
                        'active' => $this->context->id == 'ta-pra-uji',
                        'visible' => Yii::$app->user->identity->role == User::ROLE_ADMIN
                    ];
                    $items[] = [
                        'label' => '<i class="fa fa-list"></i> Judul Skripsi Final',
                        'url' => ['judul-final/index'],
                        'active' => $this->context->id == 'judul-final',
                        'visible' => Yii::$app->user->identity->role == User::ROLE_ADMIN
                    ];
                    $items[] = [
                        'label' => '<i class="fa fa-list"></i> Jadwal Ujian Judul',
                        'url' => ['jadwal-ujian/index'],
                        'active' => $this->context->id == 'jadwal-ujian',
                        'visible' => Yii::$app->user->identity->role == User::ROLE_ADMIN
                    ];
                    
                    $report_items[] = [
                        'label' => '<i class="fa fa-list"></i> Skripsi Lolos Verifikasi',
                        'url' => ['laporan/ta-lolos-verifikasi/index'],
                        'active' => $this->context->id == 'laporan/ta-lolos-verifikasi'
                    ];
                    $report_items[] = [
                        'label' => '<i class="fa fa-list"></i> Skripsi Tidak Lolos Verifikasi',
                        'url' => ['laporan/ta-tidak-lolos-verifikasi/index'],
                        'active' => $this->context->id == 'laporan/ta-tidak-lolos-verifikasi'
                    ];
                    $report_items[] = [
                        'label' => '<i class="fa fa-list"></i> Skripsi Lolos Uji',
                        'url' => ['laporan/ta-lolos-uji/index'],
                        'active' => $this->context->id == 'laporan/ta-lolos-uji'
                    ];
                    $report_items[] = [
                        'label' => '<i class="fa fa-list"></i> Skripsi Tidak Lolos Uji',
                        'url' => ['laporan/ta-tidak-lolos-uji/index'],
                        'active' => $this->context->id == 'laporan/ta-tidak-lolos-uji'
                    ];
                    $report_items[] = [
                        'label' => '<i class="fa fa-list"></i> Data Penguji',
                        'url' => ['laporan/penguji/index'],
                        'active' => $this->context->id == 'laporan/penguji'
                    ];
                    $report_items[] = [
                        'label' => '<i class="fa fa-list"></i> Data Mahasiswa',
                        'url' => ['laporan/mahasiswa/index'],
                        'active' => $this->context->id == 'laporan/mahasiswa'
                    ];

                    $items[] = [
                        'label' => '<i class="fa fa-list"></i> Laporan',
                        'options' => ['class' => 'treeview'],
                        'dropDownOptions' => ['class' => 'treeview-menu'],
                        'items' => $report_items,
                        'active' => in_array($this->context->id, ['laporan/ta-lolos-verifikasi', 'laporan/ta-tidak-lolos-verifikasi', 'laporan/ta-lolos-uji', 'laporan/ta-tidak-lolos-uji', 'laporan/penguji', 'laporan/mahasiswa']),
                        'visible' => Yii::$app->user->identity->role == User::ROLE_ADMIN
                    ];
                    
                    $user_items[] = [
                        'label' => '<i class="fa fa-user"></i> Admin',
                        'url' => ['user/admin/index'],
                        'active' => $this->context->id == 'user/admin'
                    ];
                    $user_items[] = [
                        'label' => '<i class="fa fa-user"></i> Penguji',
                        'url' => ['user/penguji/index'],
                        'active' => $this->context->id == 'user/penguji'
                    ];
                    $user_items[] = [
                        'label' => '<i class="fa fa-user"></i> Mahasiswa',
                        'url' => ['user/mahasiswa/index'],
                        'active' => $this->context->id == 'user/mahasiswa'
                    ];

                    $items[] = [
                        'label' => '<i class="fa fa-users"></i> Manajemen User',
                        'options' => ['class' => 'treeview'],
                        'dropDownOptions' => ['class' => 'treeview-menu'],
                        'items' => $user_items,
                        'active' => in_array($this->context->id, ['user/admin', 'user/penguji', 'user/mahasiswa']),
                        'visible' => Yii::$app->user->identity->role == User::ROLE_ADMIN
                    ];
                ?>
                
                <?php
                echo Nav::widget(
                    [
                        'encodeLabels' => false,
                        'options' => ['class' => 'sidebar-menu'],
                        'items' => $items
                    ]
                );
                ?>
            </section>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    <?= $this->title ?>
                    <small><?= strtoupper(Yii::$app->user->identity->role) ?> AREA</small>
                </h1>
                <?php /*Breadcrumbs::widget([
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ])*/ ?>
            </section>

            <!-- Main content -->
            <section class="content">

                <!-- Your Page Content Here -->
                <?= $content ?>

            </section><!-- /.content -->
        </div><!-- /.content-wrapper -->

        <!-- Main Footer -->
        <footer class="main-footer">
            <!-- To the right -->
            <div class="pull-right hidden-xs">
                Developed by <?= Html::a('Rizky Bintang Utama', 'https://about.me/rizkybintangutama') ?>.
            </div>
            <!-- Default to the left --> 
            <strong>Copyright &copy; 2015 <?= Html::a(Yii::$app->name, Url::base()) ?>.</strong> All rights reserved. Rendered in <?= round(Yii::getLogger()->getElapsedTime(), 2) ?>s.
        </footer>

    </div><!-- ./wrapper -->
    <!--// Dev : Rizky Bintang Utama || https://about.me/rizkybintangutama //-->
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>