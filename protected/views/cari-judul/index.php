<?php

use app\models\User;
use app\models\UserMahasiswa;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TaAjuanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cari Judul';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cari-judul-index">
    <div class="row">
        <div class="col-md-3 side-filter">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Filter</h3>
                </div>
                <div class="box-body table-responsive">
                    <?= $this->render('_search', ['data' => $data]) ?>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Daftar Judul Skripsi Pada Database Skripsi Mahasiswa</h3>
                </div>
                <div class="box-body table-responsive">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'layout' => '{items}{summary}{pager}',
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            'nama_mahasiswa',
                            'judul',
                            //'tahun_masuk',
                            'prodi',
                            'tahun_ajuan',
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</div>