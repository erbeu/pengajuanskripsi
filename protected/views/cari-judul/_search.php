<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UserMahasiswaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cari-judul-search">

    <?= Html::beginForm(['index'], 'get'); ?>

    <?= Html::label('Metode Penelitian', 'metode', ['class' => 'control-label']) ?>
    <?= Html::textInput('metode', $data['metode'], ['class' => 'form-control', 'id' => 'metode']) ?>
    <div class="help-block"></div>

    <?= Html::label('Objek Penelitian', 'objek', ['class' => 'control-label']) ?>
    <?= Html::textInput('objek', $data['objek'],  ['class' => 'form-control', 'id' => 'objek']) ?>
    <div class="help-block"></div>

    <div class="form-group">
        <?= Html::submitButton('Cari', ['class' => 'btn btn-warning btn-flat']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default btn-flat']) ?>
    </div>

    <?= Html::endForm() ?>

</div>
