<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\User;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserMahasiswaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Judul Skripsi Ajuan';
$this->params['breadcrumbs'][] = $this->title;

if(Yii::$app->request->get('page') == '') {
    $btn_class[0] = 'primary';
    $btn_class[1] = 'default';
} elseif(Yii::$app->request->get('page') == 'arsip') {
    $btn_class[0] = 'default';
    $btn_class[1] = 'primary';
}

if(Yii::$app->user->identity->role == User::ROLE_PENGUJI) {
    if(Yii::$app->request->get('page') == '') {
        $text = 'Skripsi Siap Diuji';
    } elseif(Yii::$app->request->get('page') == 'arsip') {
        $text = 'Skripsi Sudah Diuji';
    }
    $key = 'Uji';
} elseif(Yii::$app->user->identity->role == User::ROLE_ADMIN) {
    if(Yii::$app->request->get('page') == '') {
        $text = 'Skripsi Siap Diverifikasi';
    } elseif(Yii::$app->request->get('page') == 'arsip') {
        $text = 'Skripsi Belum Siap Diverifikasi';
    }
    $key = 'Verifikasi';
}
?>
<div class="ta-mahasiswa-index">
    <?php if(Yii::$app->user->identity->role == User::ROLE_PENGUJI) { ?>
        <div class="row">
            <div class="col-md-2">
                <?= Html::a('Skripsi Siap Diuji', ['index'], ['class' => 'btn btn-block btn-flat btn-'.$btn_class[0]]) ?>
            </div>
            <div class="col-md-2">
                <?= Html::a('Skripsi Sudah Diuji', ['index', 'page' => 'arsip'], ['class' => 'btn btn-block btn-flat btn-'.$btn_class[1]]) ?>
            </div>
        </div>
        <br>
        <div class="callout callout-info">
            <h4>Informasi</h4>
            <p>Seluruh judul skripsi mahasiswa <b>harus diberikan catatan oleh setiap penguji</b>.</p>
        </div>
    <?php } ?>
    <?php if(Yii::$app->user->identity->role == User::ROLE_ADMIN) { ?>
        <div class="row">
            <div class="col-md-3">
                <?= Html::a('Skripsi Siap Diverifikasi', ['index'], ['class' => 'btn btn-block btn-flat btn-'.$btn_class[0]]) ?>
            </div>
            <div class="col-md-3">
                <?= Html::a('Skripsi Belum Siap Diverifikasi', ['index', 'page' => 'arsip'], ['class' => 'btn btn-block btn-flat btn-'.$btn_class[1]]) ?>
            </div>
        </div>
        <br>
    <?php } ?>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Daftar Mahasiswa Yang Mengajukan Judul Skripsi dan <?= $text ?></h3>
                </div>
                <div class="box-body table-responsive">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'layout' => '{items}{summary}{pager}',
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            // 'id_mahasiswa',
                            // 'id_user',
                            'npm',
                            'nama_mahasiswa',
                            'kelas',
                            'semester',
                            'tahun_masuk',
                            'prodi',
                            [
                                'attribute' => 'menu',
                                'format' => 'html',
                                'options' => ['width' => '20%'],
                                'value' => function($model) use($key) {
                                    $count = 0;
                                    foreach($model->taAjuans as $data) {
                                        $count += $data->submit == 1 ? 1 : 0;
                                    }
                                    if((count($model->taAjuans)-$count) == 0) {
                                        return Html::a(ucfirst($key).' Judul', ['ta-ajuan/index', 'id_mahasiswa' => $model->id_mahasiswa], ['class' => 'btn btn-success btn-sm btn-flat']);
                                    } else {
                                        return '';
                                    }
                                }
                            ]
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</div>