<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TaAjuan */

$this->title = 'Update Skripsi Ajuan: ' . $model->idMahasiswa->nama_mahasiswa;
$this->params['breadcrumbs'][] = ['label' => 'Skripsi Ajuan', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idMahasiswa->nama_mahasiswa, 'url' => ['view', 'id' => $model->id_ajuan]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ta-ajuan-update">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Update Skripsi Ajuan</h3>
                </div>
                <div class="box-body">
                    <?= $this->render('_form', [
                        'model' => $model,
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>