<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TaAjuan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="callout callout-info">
    <h4>Informasi</h4>
    <p>Sebelum mengajukan judul skripsi, anda sebaiknya mengecek judul anda di menu <?= Html::a('<kbd>Cari Judul</kbd>', ['cari-judul/index'], ['target' => '_blank']) ?> agar tidak terdapat judul yang serupa. Jika saat verifikasi judul terdapat kesamaan dengan judul yang sudah pernah diajukan orang lain maka akan ditolak.</p>
</div>
<div class="ta-ajuan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'judul_ajuan')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Buat' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-warning btn-flat' : 'btn btn-primary btn-flat']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
