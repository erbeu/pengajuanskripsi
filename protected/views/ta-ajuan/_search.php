<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TaAjuanSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ta-ajuan-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_ajuan') ?>

    <?= $form->field($model, 'id_mahasiswa') ?>

    <?= $form->field($model, 'judul_ajuan') ?>

    <?= $form->field($model, 'tgl_ajuan') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
