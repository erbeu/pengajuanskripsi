<?php

use app\models\User;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TaAjuan */

$this->title = 'Detail Judul Skripsi Mahasiswa';
$this->params['breadcrumbs'][] = ['label' => 'Skripsi Ajuan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-ajuan-view">
    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Detail Mahasiswa</h3>
                </div>
                <div class="box-body">
                    <?= DetailView::widget([
                        'model' => $modelMahasiswa,
                        'attributes' => [
                            //'id_ajuan',
                            'npm',
                            'nama_mahasiswa',
                            'kelas',
                            'semester',
                            'tahun_masuk',
                            'prodi',
                        ],
                    ]) ?>
                </div>
                <div class="box-footer clearfix">
                    <?= Html::a('Kembali', ['index', 'id_mahasiswa' => $model->id_mahasiswa], ['class' => 'btn btn-primary btn-flat']) ?>
                </div>
            </div>
        </div>
        
        <div class="col-md-6">
            <div class="box">
                <div class="box-header with-border">
                    <i class="fa fa-list"></i>
                    <h3 class="box-title">Detail Skripsi Ajuan</h3>
                </div>
                <div class="box-body">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            //'id_ajuan',
                            [
                                'attribute' => 'Npm',
                                'value' => $model->idMahasiswa->npm,
                            ],
                            [
                                'attribute' => 'Mahasiswa',
                                'value' => $model->idMahasiswa->nama_mahasiswa,
                            ],
                            'judul_ajuan',
                            'tgl_ajuan',
                            'hasil_verifikasi',
                            'hasil_ujian'
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <?php
        if($model->hasil_verifikasi != Yii::$app->params['hasil_verifikasi'][0]) {
            ?>
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <i class="fa fa-list"></i>
                        <h3 class="box-title">Detail Verifikasi</h3>
                    </div>
                    <div class="box-body">
                        <?= DetailView::widget([
                                'model' => $modelVerifikasi,
                                'attributes' => [
                                    //'id_verifikasi',
                                    [
                                        'attribute' => 'nama_admin',
                                        'value' => $modelVerifikasi->idAdmin->nama_admin,
                                    ],
                                    'tgl_verifikasi',
                                    'catatan_verifikasi:ntext',
                                ],
                        ]) ?>
                    </div>
                </div>
            </div>
            <?php
        }
        ?>
    </div>
    <div class="row">
        <?php
        if($model->hasil_ujian != Yii::$app->params['hasil_verifikasi'][0]) {
            foreach($modelUjian as $modelUjian) {
            ?>
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header with-border">
                        <i class="fa fa-list"></i>
                        <h3 class="box-title">Detail Ujian</h3>
                    </div>
                    <div class="box-body">
                        <?= DetailView::widget([
                                'model' => $modelUjian,
                                'attributes' => [
                                    //'id_verifikasi',
                                    [
                                        'attribute' => 'nama_penguji',
                                        'value' => $modelUjian->detailUjian->idPenguji->nama_penguji,
                                    ],
                                    'catatan_penguji:ntext',
                                ],
                        ]) ?>
                    </div>
                </div>
            </div>
            <?php
            }
        }
        ?>
    </div>
</div>