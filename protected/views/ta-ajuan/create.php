<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TaAjuan */

$this->title = 'Buat Skripsi Ajuan';
$this->params['breadcrumbs'][] = ['label' => 'Skripsi Ajuan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-ajuan-create">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Buat Skripsi Ajuan</h3>
                </div>
                <div class="box-body">
                    <?= $this->render('_form', [
                        'model' => $model,
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>