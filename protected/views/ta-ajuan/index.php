<?php

use app\models\User;
use app\models\UserMahasiswa;
use app\models\UserAdmin;
use app\models\TaAjuan;
use app\models\TaVerifikasi;
use app\models\TaUjian;
use app\widgets\Alert;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TaAjuanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Skripsi Ajuan';
if(Yii::$app->user->identity->role == User::ROLE_MAHASISWA) {
    $this->title = 'Halaman Pengajuan Judul Skripsi';
}
$this->params['breadcrumbs'][] = $this->title;
?>

<?= Alert::widget() ?>

<?php if(Yii::$app->user->identity->role == User::ROLE_MAHASISWA) { ?>
    <div class="callout callout-info">
        <h4>Informasi</h4>
        <p>Setiap mahasiswa <b>WAJIB</b> mengajukan 3 judul skripsi, apabila judul skripsi yang diajukan kurang dari 3 judul maka <b>TIDAK AKAN DIVERIFIKASI</b>, dan apabila ada judul yang tidak lolos verifikasi maka mahasiswa harus mengajukan judul kembali sebanyak judul yang ditolak.</p>
        <p>Setelah anda siap dengan 3 judul anda, maka anda dapat mengirimkan judul anda dengan cara menekan tombol <kbd>Kirim Judul Skripsi</kbd>.</p>
    </div>
<?php } ?>

<div class="ta-ajuan-index">
    <?php if($dataProviderDetailUjian->totalCount != 0) { ?>
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Detail Ujian</h3>
                    </div>
                    <div class="box-body table-responsive">
                        <?= GridView::widget([
                            'dataProvider' => $dataProviderDetailUjian,
                            'layout' => '{items}{summary}{pager}',
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],

                                //'id_ujian',
                                [
                                    'attribute' => 'nama_penguji',
                                    'value' => 'idPenguji.nama_penguji'
                                ],
                                [
                                    'attribute' => 'tgl_ujian',
                                    'options' => ['width' => '15%'],
                                ],
                                [
                                    'attribute' => 'ruang',
                                    'options' => ['width' => '10%'],
                                ],
                            ],
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    <div class="row">
        <div class="col-md-4">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Detail Mahasiswa</h3>
                </div>
                <div class="box-body">
                    <?= DetailView::widget([
                        'model' => $modelMahasiswa,
                        'attributes' => [
                            //'id_ajuan',
                            'npm',
                            'nama_mahasiswa',
                            'kelas',
                            'semester',
                            'tahun_masuk',
                            'prodi',
                        ],
                    ]) ?>
                </div>
                <div class="box-footer clearfix">
                    <?php
                    if(Yii::$app->user->identity->role == User::ROLE_ADMIN) {
                        $query = UserMahasiswa::find()
                            ->joinWith(['taAjuans'])
                            ->groupBy('user_mahasiswa.id_mahasiswa')
                            ->where(['user_mahasiswa.id_mahasiswa' => $modelMahasiswa['id_mahasiswa'], 'ta_ajuan.submit' => 1, 'ta_ajuan.hasil_verifikasi' => Yii::$app->params['hasil_verifikasi'][0]])
                            ->count();
                        $back = $query == 1 ? 'ta-mahasiswa' : 'ta-pra-uji';
                        if($query == 0) {
                            $query = UserMahasiswa::find()
                                ->joinWith(['detailUjian', 'taAjuans'])
                                ->where(['user_mahasiswa.id_mahasiswa' => $modelMahasiswa['id_mahasiswa'], 'ta_ajuan.hasil_verifikasi' => Yii::$app->params['hasil_verifikasi'][1]])
                                ->groupBy('user_mahasiswa.id_mahasiswa')
                                ->having('count(ta_ajuan.hasil_verifikasi) = 3 AND count(detail_ujian.id_mahasiswa)/2 < 3')
                                ->count();
                            $back = $query == 1 ? 'ta-lolos-verifikasi' : 'ta-pra-uji';
                        }
                        echo Html::a('Kembali', [$back.'/index'], ['class' => 'btn btn-primary btn-flat']);
                    } elseif(Yii::$app->user->identity->role == User::ROLE_PENGUJI) {
                        echo Html::a('Kembali', ['ta-mahasiswa/index'], ['class' => 'btn btn-primary btn-flat']);
                    }
                    ?>
                </div>
            </div>
        </div>
        
        <div class="col-md-8">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Detail Skripsi Ajuan</h3>
                </div>
                <div class="box-body table-responsive">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        //'filterModel' => $searchModel,
                        'layout' => '{items}{summary}{pager}',
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            //'id_ajuan',
                            [
                                'attribute' => 'judul_ajuan',
                                'options' => ['width' => '30%'],
                            ],
                            'tgl_ajuan',
                            [
                                'attribute' => 'verifikasi',
                                'format' => 'html',
                                'value' => function($model) {
                                    $class = 'warning';
                                    if($model->hasil_verifikasi == Yii::$app->params['hasil_verifikasi'][1]) {
                                        $class = 'success';
                                    } elseif($model->hasil_verifikasi == Yii::$app->params['hasil_verifikasi'][2]) {
                                        $class = 'danger';
                                    }
                                    $return = '<span class="label label-'.$class.'">'.$model->hasil_verifikasi.'</span>';

                                    return $return;
                                }
                            ],
                            [
                                'attribute' => 'ujian',
                                'format' => 'html',
                                'value' => function($model) {
                                    $class = 'warning';
                                    if($model->hasil_ujian == Yii::$app->params['hasil_ujian'][1]) {
                                        $class = 'success';
                                    } elseif($model->hasil_ujian == Yii::$app->params['hasil_ujian'][2]) {
                                        $class = 'danger';
                                    }
                                    $return = '<span class="label label-'.$class.'">'.$model->hasil_ujian.'</span>';

                                    return $return;
                                }
                            ],
                            [
                                'attribute' => '',
                                'format' => 'html',
                                'value' => function($model) {
                                    $return = '<div class="btn-group-vertical">';
                                    if($model->hasil_verifikasi == Yii::$app->params['hasil_verifikasi'][0] &&
                                       Yii::$app->user->identity->role == User::ROLE_ADMIN) {
                                        $return .= Html::a('Buat Verifikasi', ['ta-verifikasi/create', 'id_ajuan' => $model->id_ajuan], ['class' => 'btn btn-success btn-sm btn-flat']);
                                    } elseif($model->hasil_verifikasi != Yii::$app->params['hasil_verifikasi'][0] &&
                                        Yii::$app->user->identity->role == User::ROLE_ADMIN &&
                                        Yii::$app->user->id_admin == $model->taVerifikasi->id_admin &&
                                        $model->taVerifikasi->submit == 0) {
                                        $return .= Html::a('Edit Verifikasi', ['ta-verifikasi/update', 'id_ajuan' => $model->id_ajuan], ['class' => 'btn btn-success btn-sm btn-flat']);
                                    }
                                    if(Yii::$app->user->identity->role == User::ROLE_PENGUJI) {
                                        $return .= Html::a('Lihat Ujian', ['ta-ujian/index', 'id_ajuan' => $model->id_ajuan], ['class' => 'btn btn-success btn-sm btn-flat']);
                                    }
                                    $return .= Html::a('Detail', ['view', 'id' => $model->id_ajuan], ['class' => 'btn btn-success btn-sm btn-flat']);
                                    $return .= '</div>';
                                    return $return;
                                }
                            ],

                            [
                                'class' => 'yii\grid\ActionColumn',
                                'visibleButtons' => [
                                    'view' => false,
                                    'update' => function($model) {
                                        return (Yii::$app->user->identity->role == User::ROLE_MAHASISWA && $model->submit == 0) ? true : false;
                                    },
                                    'delete' => false,
                                ]
                            ],
                        ],
                    ]); ?>
                </div>
                <div class="box-footer clearfix">
                    <?php
                    if(Yii::$app->user->identity->role == User::ROLE_MAHASISWA) {
                        $ta_verifikasi = TaVerifikasi::find()
                            ->joinWith(['idAjuan'])
                            ->where(['ta_ajuan.id_mahasiswa' => Yii::$app->user->id_mahasiswa, 'ta_verifikasi.submit' => 1])
                            ->count();
                        $jml_ta_tolak = TaAjuan::find()
                            ->where(['id_mahasiswa' => Yii::$app->user->id_mahasiswa, 'hasil_verifikasi' => 'ditolak'])
                            ->count();
                        $submit = TaAjuan::find()
                            ->where(['id_mahasiswa' => Yii::$app->user->id_mahasiswa, 'submit' => 0])
                            ->count();
                        if(($dataProvider->totalCount-$jml_ta_tolak) < 3 && $ta_verifikasi > 0) {
                            echo Html::a('Tambah Judul Skripsi', ['create'], ['class' => 'btn btn-warning btn-flat']);
                        } elseif(($dataProvider->totalCount-$jml_ta_tolak) == 3 and $submit > 0) {
                            echo Html::a('Kirim Judul Skripsi', ['send'], ['class' => 'btn btn-primary btn-flat']);
                        }
                    }
                    
                    if(Yii::$app->user->identity->role == User::ROLE_ADMIN) {
                        $ta = TaAjuan::find()
                            ->where(['id_mahasiswa' => $modelMahasiswa['id_mahasiswa'], 'hasil_verifikasi' => 'belum diverifikasi'])
                            ->count();
                        $submit = TaAjuan::find()
                            ->joinWith(['taVerifikasi'])
                            ->where(['id_mahasiswa' => $modelMahasiswa['id_mahasiswa'], 'ta_verifikasi.submit' => 0])
                            ->count();
                        if($ta == 0 && $submit > 0) {
                            echo Html::a('Kirim Hasil Verifikasi', ['ta-verifikasi/send', 'id_mahasiswa' => $modelMahasiswa['id_mahasiswa']], ['class' => 'btn btn-primary btn-flat']);
                        }
                    }
                    
                    if(Yii::$app->user->identity->role == User::ROLE_PENGUJI) {
                        $ta = TaAjuan::find()
                            ->where(['id_mahasiswa' => $modelMahasiswa['id_mahasiswa'], 'hasil_verifikasi' => 'diterima', 'hasil_ujian' => 'belum diuji'])
                            ->count();
                        $submit = TaUjian::find()
                            ->joinWith(['detailUjian'])
                            ->where(['detail_ujian.id_mahasiswa' => $modelMahasiswa['id_mahasiswa'], 'detail_ujian.id_penguji' => Yii::$app->user->id_penguji, 'submit' => 0])
                            ->count();
                        if($ta == 0 && $submit == 3) {
                            echo Html::a('Kirim Hasil Ujian', ['ta-ujian/send', 'id_mahasiswa' => $modelMahasiswa['id_mahasiswa']], ['class' => 'btn btn-primary btn-flat']);
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>