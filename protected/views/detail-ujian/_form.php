<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\DetailUjian */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="detail-ujian-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_penguji')->dropDownList(ArrayHelper::map($modelPenguji, 'id_penguji', 'nama_penguji'), ['prompt' => '']) ?>

    <?= $form->field($model, 'tgl_ujian')->textInput(['type' => 'date']) ?>

    <?= $form->field($model, 'ruang')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Buat' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-warning btn-flat' : 'btn btn-primary btn-flat']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
