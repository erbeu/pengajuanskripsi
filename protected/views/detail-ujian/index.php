<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\DetailView;
use app\models\User;
use app\models\UserMahasiswa;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DetailUjianSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Detail Ujian';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="detail-ujian-index">
    <div class="row">
        <div class="col-md-4">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Detail Mahasiswa</h3>
                </div>
                <div class="box-body">
                    <?= DetailView::widget([
                        'model' => $modelMahasiswa,
                        'attributes' => [
                            //'id_ajuan',
                            'npm',
                            'nama_mahasiswa',
                            'kelas',
                            'semester',
                            'tahun_masuk',
                            'prodi',
                        ],
                    ]) ?>
                </div>
                <div class="box-footer clearfix">
                    <?php
                    $query = UserMahasiswa::find()
                        ->joinWith(['detailUjian', 'taAjuans'])
                        ->where(['user_mahasiswa.id_mahasiswa' => $modelMahasiswa['id_mahasiswa'], 'ta_ajuan.hasil_verifikasi' => Yii::$app->params['hasil_verifikasi'][1]])
                        ->groupBy('user_mahasiswa.id_mahasiswa')
                        ->having('count(ta_ajuan.hasil_verifikasi) = 3 AND count(detail_ujian.id_mahasiswa)/2 < 3')
                        ->count();

                    echo Html::a('Kembali', ['ta-'.($query == 1 ? 'lolos-verifikasi' : 'pra-uji').'/index'], ['class' => 'btn btn-primary btn-flat']);
                    ?>
                </div>
            </div>
        </div>
        
        <div class="col-md-8">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Daftar Detail Ujian</h3>
                </div>
                <div class="box-body table-responsive">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        //'filterModel' => $searchModel,
                        'layout' => '{items}{summary}{pager}',
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            //'id_ujian',
                            [
                                'attribute' => 'nama_penguji',
                                'value' => 'idPenguji.nama_penguji'
                            ],
                            [
                                'attribute' => 'tgl_ujian',
                                'options' => ['width' => '15%'],
                            ],
                            [
                                'attribute' => 'ruang',
                                'options' => ['width' => '10%'],
                            ],

                            [
                                'class' => 'yii\grid\ActionColumn',
                                'options' => ['width' => '10%'],
                                'visibleButtons' => [
                                    'view' => false
                                ]
                            ],
                        ],
                    ]); ?>
                </div>
                <div class="box-footer clearfix">
                    <?php
                    if(Yii::$app->user->identity->role == User::ROLE_ADMIN) {
                        if(($dataProvider->totalCount) < 2) {
                            echo Html::a('Buat Detail Ujian', ['create', 'id_mahasiswa' => $modelMahasiswa['id_mahasiswa']], ['class' => 'btn btn-warning btn-flat']);
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>