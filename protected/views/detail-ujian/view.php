<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\DetailUjian */

$this->title = 'Detail Ujian';
$this->params['breadcrumbs'][] = ['label' => 'Detail Ujian', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="detail-ujian-view">
    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Detail Mahasiswa</h3>
                </div>
                <div class="box-body">
                    <?= DetailView::widget([
                        'model' => $modelMahasiswa,
                        'attributes' => [
                            //'id_ajuan',
                            'npm',
                            'nama_mahasiswa',
                            'kelas',
                            'semester',
                            'tahun_masuk',
                            'prodi',
                        ],
                    ]) ?>
                </div>
                <div class="box-footer clearfix">
                    <?= Html::a('Kembali', ['index', 'id_mahasiswa' => $modelMahasiswa['id_mahasiswa']], ['class' => 'btn btn-primary btn-flat']) ?>
                </div>
            </div>
        </div>
        
        <div class="col-md-6">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Detail Ujian</h3>
                </div>
                <div class="box-body">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'id_detail',
                            [
                                'attribute' => 'nama_penguji',
                                'value' => $model->idPenguji->nama_penguji,
                            ],
                            'tgl_ujian',
                            'ruang',
                            [
                                'attribute' => 'nama_admin',
                                'value' => $model->idAdmin->nama_admin,
                            ],
                        ],
                    ]) ?>
                </div>
                <div class="box-footer clearfix">
                    <?= Html::a('Update', ['update', 'id' => $model->id_detail], ['class' => 'btn btn-primary btn-flat']) ?>
                    <?= Html::a('Hapus', ['delete', 'id' => $model->id_detail], [
                            'class' => 'btn btn-danger btn-flat',
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                            ],
                        ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>