<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\DetailUjian */

$this->title = 'Buat Detail Ujian';
$this->params['breadcrumbs'][] = ['label' => 'Detail Ujian', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="detail-ujian-create">
    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Detail Mahasiswa</h3>
                </div>
                <div class="box-body">
                    <?= DetailView::widget([
                        'model' => $modelMahasiswa,
                        'attributes' => [
                            //'id_ajuan',
                            'npm',
                            'nama_mahasiswa',
                            'kelas',
                            'semester',
                            'tahun_masuk',
                            'prodi',
                        ],
                    ]) ?>
                </div>
                <div class="box-footer clearfix">
                    <?= Html::a('Kembali', ['index', 'id_mahasiswa' => $modelMahasiswa['id_mahasiswa']], ['class' => 'btn btn-primary btn-flat']) ?>
                </div>
            </div>
        </div>
        
        <div class="col-md-6">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Buat Detail Ujian</h3>
                </div>
                <div class="box-body">
                    <?= $this->render('_form', [
                        'model' => $model,
                        'modelPenguji' => $modelPenguji,
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>