<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DetailUjianSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="detail-ujian-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_ujian') ?>

    <?= $form->field($model, 'id_penguji') ?>

    <?= $form->field($model, 'id_mahasiswa') ?>

    <?= $form->field($model, 'tgl_ujian') ?>

    <?= $form->field($model, 'ruang') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
