<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\UserPenguji */

$this->title = 'Buat Penguji';
$this->params['breadcrumbs'][] = ['label' => 'Penguji', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-penguji-create">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <?= $this->render('_form', [
                        'model' => $model,
                        'user' => $user,
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>