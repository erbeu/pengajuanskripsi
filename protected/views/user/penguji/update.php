<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\UserPenguji */

$this->title = 'Update Penguji: ' . $model->nama_penguji;
$this->params['breadcrumbs'][] = ['label' => 'Penguji', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nama_penguji, 'url' => ['view', 'id' => $model->id_penguji]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-penguji-update">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <?= $this->render('_form', [
                        'model' => $model,
                        'user' => $user,
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>