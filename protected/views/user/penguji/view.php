<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\UserPenguji */

$this->title = $model->nama_penguji;
$this->params['breadcrumbs'][] = ['label' => 'Penguji', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-penguji-view">
    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <div class="box-header with-border">
                    <i class="fa fa-list"></i>
                    <h3 class="box-title">Detail Penguji</h3>
                </div>
                <div class="box-body">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            //'id_penguji',
                            //'id_user',
                            'nik',
                            'nama_penguji',
                            'jenis_kelamin',
                            'jafung',
                        ],
                    ]) ?>
                </div>
                <div class="box-footer clearfix">
                    <?= Html::a('Update', ['update', 'id' => $model->id_penguji], ['class' => 'btn btn-primary btn-flat']) ?>
                    <?php
                    if(Yii::$app->user->identity->role == User::ROLE_ADMIN) {
                        echo Html::a('Hapus', ['delete', 'id' => $model->id_penguji], [
                            'class' => 'btn btn-danger btn-flat',
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                            ],
                        ]);
                    }
                    ?>
                </div>
            </div>
        </div>
        
        <div class="col-md-6">
            <div class="box">
                <div class="box-header with-border">
                    <i class="fa fa-list"></i>
                    <h3 class="box-title">Detail User</h3>
                </div>
                <div class="box-body">
                    <?= DetailView::widget([
                        'model' => $user,
                        'attributes' => [
                            'username',
                            'role',
                            [
                                'label' => 'Created_by',
                                'value' => $user->createdBy->username,
                            ],
                            'created_at',
                            [
                                'label' => 'Updated_by',
                                'value' => $user->updatedBy->username,
                            ],
                            'updated_at',
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>