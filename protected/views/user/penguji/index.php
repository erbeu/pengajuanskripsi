<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserPengujiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Manajemen User Penguji';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-penguji-index">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body table-responsive">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'layout' => '{items}{summary}{pager}',
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            //'id_penguji',
                            //'id_user',
                            'nik',
                            'nama_penguji',
                            'jenis_kelamin',
                            'jafung',

                            [
                                'class' => 'yii\grid\ActionColumn',
                                'options' => ['width' => '10%'],
                            ],
                        ],
                    ]); ?>
                </div>
                <div class="box-footer clearfix">
                    <?= Html::a('Buat Penguji', ['create'], ['class' => 'btn btn-warning btn-flat']) ?>
                </div>
            </div>
        </div>
    </div>
</div>