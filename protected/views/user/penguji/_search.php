<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UserPengujiSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-penguji-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_penguji') ?>

    <?= $form->field($model, 'id_user') ?>

    <?= $form->field($model, 'nik') ?>

    <?= $form->field($model, 'nama_penguji') ?>

    <?= $form->field($model, 'jafung') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
