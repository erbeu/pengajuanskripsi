<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UserPenguji */
/* @var $form yii\widgets\ActiveForm */

$list_kelamin = ['L' => 'Laki - Laki', 'P' => 'Perempuan'];
?>

<div class="user-penguji-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $model->isNewRecord ? $form->field($model, 'nik')->textInput(['maxlength' => true]) : '' ?>

    <?= $form->field($model, 'nama_penguji')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'jenis_kelamin')->dropDownList($list_kelamin, ['prompt' => '']) ?>

    <?= $form->field($model, 'jafung')->textInput(['maxlength' => true]) ?>

    <?= !$model->isNewRecord ? $form->field($user, 'pass')->passwordInput().'<p class="help-block">Kosongkan password jika tidak ingin diganti.</p>' : '' ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Buat' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-warning btn-flat' : 'btn btn-primary btn-flat']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
