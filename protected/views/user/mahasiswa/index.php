<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserMahasiswaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Manajemen User Mahasiswa';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-mahasiswa-index">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body table-responsive">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'layout' => '{items}{summary}{pager}',
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            // 'id_mahasiswa',
                            // 'id_user',
                            'npm',
                            'nama_mahasiswa',
                            'jenis_kelamin',
                            'kelas',
                            'semester',
                            'tahun_masuk',
                            'prodi',

                            [
                                'class' => 'yii\grid\ActionColumn',
                                'options' => ['width' => '10%'],
                            ],
                        ],
                    ]); ?>
                </div>
                <div class="box-footer clearfix">
                    <?= Html::a('Buat Mahasiswa', ['create'], ['class' => 'btn btn-warning btn-flat']) ?>
                </div>
            </div>
        </div>
    </div>
</div>