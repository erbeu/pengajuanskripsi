<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UserMahasiswa */
/* @var $form yii\widgets\ActiveForm */

$list_kelas = array_combine(Yii::$app->params['list_kelas'], Yii::$app->params['list_kelas']);
$list_semester = array_combine(Yii::$app->params['list_semester'], Yii::$app->params['list_semester']);
$list_prodi = array_combine(Yii::$app->params['list_prodi'], Yii::$app->params['list_prodi']);
$list_tahun = array_combine($data['list_tahun'], $data['list_tahun']);
$list_kelamin = ['L' => 'Laki - Laki', 'P' => 'Perempuan'];
?>

<div class="user-mahasiswa-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $model->isNewRecord ? $form->field($model, 'npm')->textInput(['maxlength' => true]) : '' ?>

    <?= $form->field($model, 'nama_mahasiswa')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'jenis_kelamin')->dropDownList($list_kelamin, ['prompt' => '']) ?>

    <?= $form->field($model, 'kelas')->dropDownList($list_kelas, ['prompt' => '']) ?>

    <?= $form->field($model, 'semester')->dropDownList($list_semester, ['prompt' => '']) ?>

    <?= $form->field($model, 'tahun_masuk')->dropDownList($list_tahun, ['prompt' => '']) ?>

    <?= $form->field($model, 'prodi')->dropDownList($list_prodi, ['prompt' => '']) ?>
    
    
    <?= !$model->isNewRecord ? $form->field($user, 'pass')->passwordInput().'<p class="help-block">Kosongkan password jika tidak ingin diganti.</p>' : '' ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Buat' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-warning btn-flat' : 'btn btn-primary btn-flat']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
