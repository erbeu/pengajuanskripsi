<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\UserMahasiswa */

$this->title = 'Buat Mahasiswa';
$this->params['breadcrumbs'][] = ['label' => 'Mahasiswa', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-mahasiswa-create">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <?= $this->render('_form', [
                        'model' => $model,
                        'user' => $user,
                        'data' => $data,
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>