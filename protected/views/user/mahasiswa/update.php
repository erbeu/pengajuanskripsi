<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\UserMahasiswa */

$this->title = 'Update Mahasiswa: ' . $model->nama_mahasiswa;
$this->params['breadcrumbs'][] = ['label' => 'Mahasiswa', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nama_mahasiswa, 'url' => ['view', 'id' => $model->id_mahasiswa]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-mahasiswa-update">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <?= $this->render('_form', [
                        'model' => $model,
                        'user' => $user,
                        'data' => $data,
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>