<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\UserAdmin */

$this->title = 'Update Admin: ' . $model->nama_admin;
$this->params['breadcrumbs'][] = ['label' => 'Admin', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nama_admin, 'url' => ['view', 'id' => $model->id_admin]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-admin-update">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <?= $this->render('_form', [
                        'model' => $model,
                        'user' => $user,
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>