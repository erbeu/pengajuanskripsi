<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserAdminSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Manajemen User Admin';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-admin-index">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body table-responsive">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'layout' => '{items}{summary}{pager}',
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            //'id_admin',
                            //'id_user',
                            [
                                'attribute' => 'id_user',
                                'value' => 'idUser.username',
                            ],
                            'nama_admin',
                            'jenis_kelamin',
                            'jabatan',

                            [
                                'class' => 'yii\grid\ActionColumn',
                                'options' => ['width' => '10%'],
                            ],
                        ],
                    ]); ?>
                </div>
                <div class="box-footer clearfix">
                    <?= Html::a('Buat Admin', ['create'], ['class' => 'btn btn-warning btn-flat']) ?>
                </div>
            </div>
        </div>
    </div>
</div>