<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Rizky Bintang Utama <rizkybintangutama@gmail.com>
 * @since 2.0
 */
class AdminLTEAsset extends AssetBundle
{
    public $basePath = '@webroot/assets-static/AdminLTE';
    public $baseUrl = '@web/assets-static/AdminLTE';
    public $css = [
        'dist/css/AdminLTE.min.css',
        'dist/css/skins/_all-skins.min.css',
        'plugins/iCheck/square/blue.css',
        'plugins/font-awesome/font-awesome.min.css',
        '../css/web.css',
    ];
    public $js = [
        'dist/js/app.min.js',
        'plugins/iCheck/icheck.min.js',
        '../ckeditor/ckeditor.js',
        '../js/clipboard.min.js',
        '../js/grid-a-licious.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
