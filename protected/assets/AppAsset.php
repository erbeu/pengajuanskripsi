<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'assets-static/css/web.css',
    ];
    public $js = [
        'assets-static/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js',
        'assets-static/js/clipboard.min.js'
    ];
    public $depends = [
        'app\assets\AdminLTEAsset',
    ];
}
