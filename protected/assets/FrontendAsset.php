<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Rizky Bintang Utama <rizkybintangutama@gmail.com>
 * @since 2.0
 */
class FrontendAsset extends AssetBundle
{
    public $basePath = '@webroot/assets-static/';
    public $baseUrl = '@web/assets-static/';
    public $css = [
        'bootstrap-material-design/dist/css/bootstrap-material-design.min.css',
        'bootstrap-material-design/dist/css/ripples.min.css',
        'css/frontend.css',
    ];
    public $js = [
        'bootstrap-material-design/dist/js/material.min.js',
        'bootstrap-material-design/dist/js/ripples.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
