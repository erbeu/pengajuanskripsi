<?php

namespace app\controllers\user;

use Yii;
use app\models\UserPenguji;
use app\models\UserPengujiSearch;
use app\models\User;
use app\components\AccessRule;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * PengujiController implements the CRUD actions for UserPenguji model.
 */
class PengujiController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'allow' => true,
                        'roles' => [
                            User::ROLE_ADMIN,
                        ],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all UserPenguji models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserPengujiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single UserPenguji model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
            'user' => User::findOne($this->findModel($id)['id_user']),
        ]);
    }

    /**
     * Creates a new UserPenguji model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UserPenguji();
        $user = new User();

        if ($model->load(Yii::$app->request->post())) {
            $user->username = $model->nik;
            $user->role = User::ROLE_PENGUJI;
            $user->setPassword($model->nik);
            $user->generateAuthKey();
            if($user->save(false)) {
                $model->id_user = $user->id_user;
                $model->save();
                return $this->redirect(['view', 'id' => $model->id_penguji]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
                'user' => $user,
            ]);
        }
    }

    /**
     * Updates an existing UserPenguji model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $user = User::findOne($this->findModel($id)['id_user']);

        if ($model->load(Yii::$app->request->post()) && $user->load(Yii::$app->request->post())) {
            if(!empty($user->pass)) {
                $user->setPassword($user->pass);
                $user->generateAuthKey();
            }
            if($user->save(false)) {
                $model->save();
                return $this->redirect(['view', 'id' => $model->id_mahasiswa]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
                'user' => $user,
            ]);
        }
    }

    /**
     * Deletes an existing UserPenguji model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $user = User::findOne($this->findModel($id)['id_user']);
        $this->findModel($id)->delete();
        $user->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the UserPenguji model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UserPenguji the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UserPenguji::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
