<?php

namespace app\controllers\user;

use Yii;
use app\models\UserMahasiswa;
use app\models\UserMahasiswaSearch;
use app\models\User;
use app\components\AccessRule;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * MahasiswaController implements the CRUD actions for UserMahasiswa model.
 */
class MahasiswaController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'allow' => true,
                        'roles' => [
                            User::ROLE_ADMIN,
                        ],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all UserMahasiswa models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserMahasiswaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single UserMahasiswa model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
            'user' => User::findOne($this->findModel($id)['id_user']),
        ]);
    }

    /**
     * Creates a new UserMahasiswa model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UserMahasiswa(['scenario' => 'create']);
        $user = new User();
        
        for($i = date('Y'); $i >= (date('Y')-10); $i--) { $data['list_tahun'][] = $i; }

        if ($model->load(Yii::$app->request->post())) {
            $user->username = $model->npm;
            $user->role = User::ROLE_MAHASISWA;
            $user->setPassword($model->npm);
            $user->generateAuthKey();
            if($user->save(false)) {
                $model->id_user = $user->id_user;
                if($model->save()) {
                    return $this->redirect(['view', 'id' => $model->id_mahasiswa]);
                } else {
                    return $this->render('create', [
                        'model' => $model,
                        'user' => $user,
                        'data' => $data,
                    ]);
                }
            }
        } else {
            return $this->render('create', [
                'model' => $model,
                'user' => $user,
                'data' => $data,
            ]);
        }
    }

    /**
     * Updates an existing UserMahasiswa model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $user = User::findOne($this->findModel($id)['id_user']);
        
        for($i = date('Y'); $i >= (date('Y')-10); $i--) { $data['list_tahun'][] = $i; }

        if ($model->load(Yii::$app->request->post()) && $user->load(Yii::$app->request->post())) {
            if(!empty($user->pass)) {
                $user->setPassword($user->pass);
                $user->generateAuthKey();
            }
            if($user->save(false)) {
                if($model->save()) {
                    return $this->redirect(['view', 'id' => $model->id_mahasiswa]);
                } else {
                    return $this->render('update', [
                        'model' => $model,
                        'user' => $user,
                        'data' => $data,
                    ]);
                }
            }
        } else {
            return $this->render('update', [
                'model' => $model,
                'user' => $user,
                'data' => $data,
            ]);
        }
    }

    /**
     * Deletes an existing UserMahasiswa model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $user = User::findOne($this->findModel($id)['id_user']);
        $this->findModel($id)->delete();
        $user->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the UserMahasiswa model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UserMahasiswa the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UserMahasiswa::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
