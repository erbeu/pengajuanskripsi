<?php

namespace app\controllers;

use Yii;
use app\models\TaUjian;
use app\models\TaUjianSearch;
use app\models\TaAjuan;
use app\models\DetailUjian;
use app\models\JudulFinal;
use app\models\User;
use app\models\UserPenguji;
use app\models\UserMahasiswa;
use app\components\AccessRule;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * TaUjianController implements the CRUD actions for TaUjian model.
 */
class TaUjianController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'send'],
                        'allow' => true,
                        'roles' => [
                            User::ROLE_PENGUJI,
                        ],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TaUjian models.
     * @return mixed
     */
    public function actionIndex($id_ajuan)
    {
        $searchModel = new TaUjianSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, ['id_ajuan' => $id_ajuan]);
        $modelTaAjuan = TaAjuan::findOne($id_ajuan);
        $modelPenguji = UserPenguji::findOne(['id_user' => Yii::$app->user->identity->id_user]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'modelTaAjuan' => $modelTaAjuan,
            'modelPenguji' => $modelPenguji,
        ]);
    }
    
    public function actionSend($id_mahasiswa) {
        $ta = TaAjuan::find()
            ->where(['id_mahasiswa' => $id_mahasiswa, 'hasil_verifikasi' => 'diterima', 'hasil_ujian' => 'belum diuji'])
            ->count();
        $submit = TaUjian::find()
            ->joinWith(['detailUjian'])
            ->where(['detail_ujian.id_mahasiswa' => $id_mahasiswa, 'detail_ujian.id_penguji' => Yii::$app->user->id_penguji, 'ta_ujian.submit' => 0])
            ->count();
        if($ta == 0 && $submit == 3) {
            $q = TaUjian::find()
                ->joinWith(['detailUjian'])
                ->where(['detail_ujian.id_mahasiswa' => $id_mahasiswa, 'detail_ujian.id_penguji' => Yii::$app->user->id_penguji, 'ta_ujian.pemberi_hasil' => 1])
                ->count();
            if($q == 3) {
                $modelMahasiswa = UserMahasiswa::findOne($id_mahasiswa);
                $modelTaAjuan = TaAjuan::findOne(['id_mahasiswa' => $id_mahasiswa, 'hasil_ujian' => 'diterima']);

                $judul_final = new JudulFinal();
                $judul_final->npm = $modelMahasiswa['npm'];
                $judul_final->nama_mahasiswa = $modelMahasiswa['nama_mahasiswa'];
                $judul_final->jenis_kelamin = $modelMahasiswa['jenis_kelamin'];
                $judul_final->kelas = $modelMahasiswa['kelas'];
                $judul_final->tahun_masuk = $modelMahasiswa['tahun_masuk'];
                $judul_final->prodi = $modelMahasiswa['prodi'];
                $judul_final->id_ajuan = $modelTaAjuan['id_ajuan'];
                $judul_final->judul = $modelTaAjuan['judul_ajuan'];
                $judul_final->tahun_ajuan = date('Y', strtotime($modelTaAjuan['tgl_ajuan']));
                $judul_final->insert();
            }
            
            $model = Yii::$app->db->createCommand('UPDATE ta_ujian LEFT JOIN detail_ujian ON detail_ujian.id_detail = ta_ujian.id_detail SET ta_ujian.submit = 1 WHERE detail_ujian.id_mahasiswa = '.$id_mahasiswa.' AND detail_ujian.id_penguji = '.Yii::$app->user->id_penguji)->execute();
            if($model) {
                Yii::$app->session->setFlash('warning', 'Hasil ujian berhasil dikirim');
            } else {
                Yii::$app->session->setFlash('danger', 'Hasil ujian gagal dikirim');
            }
        } else {
            Yii::$app->session->setFlash('danger', 'Hayooo ngapain');
        }
        return $this->redirect(['ta-ajuan/index', 'id_mahasiswa' => $id_mahasiswa]);
    }

    /**
     * Displays a single TaUjian model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TaUjian model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id_ajuan)
    {
        $model = new TaUjian();
        
        $modelTaAjuan = TaAjuan::findOne($id_ajuan);
        $modelMahasiswa = UserMahasiswa::findOne($modelTaAjuan['id_mahasiswa']);
        $modelDetailUjian = DetailUjian::findOne([
            'id_penguji' => Yii::$app->user->id_penguji,
            'id_mahasiswa' => $modelMahasiswa['id_mahasiswa'],
        ]);
        
        $condition = TaUjian::find()
            ->join('LEFT JOIN', 'detail_ujian', 'detail_ujian.id_detail = ta_ujian.id_detail')
            ->where(['id_penguji' => Yii::$app->user->id_penguji, 'id_ajuan' => $id_ajuan])
            ->exists();
        if(!$condition) {
            if ($model->load(Yii::$app->request->post())) {
                $q = TaUjian::find()->where(['id_ajuan' => $id_ajuan])->count();
                if($q == 0) {
                    $model->pemberi_hasil = 1;
                }
                if($modelTaAjuan['hasil_ujian'] == 'belum diuji') {
                    $modelTaAjuan->load(Yii::$app->request->post());
                    if($modelTaAjuan->hasil_ujian == 'diterima') {
                        TaAjuan::updateAll(
                            ['hasil_ujian' => Yii::$app->params['hasil_ujian'][2]],
                            "hasil_verifikasi = '".Yii::$app->params['hasil_verifikasi'][1]."' AND id_ajuan != '".$id_ajuan."' AND id_mahasiswa = '".$modelTaAjuan->id_mahasiswa."'"
                        );
                        $update = Yii::$app->db->createCommand('UPDATE ta_ujian LEFT JOIN ta_ajuan ON ta_ajuan.id_ajuan = ta_ujian.id_ajuan SET ta_ujian.pemberi_hasil = 1 WHERE ta_ajuan.id_mahasiswa = '.$modelMahasiswa['id_mahasiswa'])->execute();
                    }
                    $modelTaAjuan->save(false);
                }
                $model->id_detail = $modelDetailUjian['id_detail'];
                $model->id_ajuan = $id_ajuan;
                if($model->save()) {
                    return $this->redirect(['index', 'id_ajuan' => $id_ajuan]);
                }
                /*
                if($modelTaAjuan->hasil_ujian == Yii::$app->params['hasil_ujian'][0]) {
                    $modelTaAjuan->hasil_ujian = Yii::$app->params['hasil_ujian'][Yii::$app->request->post('TaAjuan')['hasil_ujian']];

                    if($model->save(false) && $modelTaAjuan->save(false)) {

                        if(!empty($modelTaAjuan->hasil_ujian) && $modelTaAjuan->hasil_ujian == Yii::$app->params['hasil_ujian'][1]) {

                            $judul_final = new JudulFinal();
                            $judul_final->npm = $modelMahasiswa['npm'];
                            $judul_final->nama_mahasiswa = $modelMahasiswa['nama_mahasiswa'];
                            $judul_final->jenis_kelamin = $modelMahasiswa['jenis_kelamin'];
                            $judul_final->kelas = $modelMahasiswa['kelas'];
                            $judul_final->tahun_masuk = $modelMahasiswa['tahun_masuk'];
                            $judul_final->prodi = $modelMahasiswa['prodi'];
                            $judul_final->id_ajuan = $modelTaAjuan->id_ajuan;
                            $judul_final->judul = $modelTaAjuan->judul_ajuan;
                            $judul_final->tahun_ajuan = date('Y', strtotime($modelTaAjuan->tgl_ajuan));
                            $judul_final->insert();

                            TaAjuan::updateAll(
                                ['hasil_ujian' => Yii::$app->params['hasil_ujian'][2]],
                                "hasil_verifikasi = '".Yii::$app->params['hasil_verifikasi'][1]."' AND id_ajuan != '".$modelTaAjuan->id_ajuan."' AND id_mahasiswa = '".$modelTaAjuan->id_mahasiswa."'"
                            );
                        }
                    } else {
                        return $this->render('create', [
                            'model' => $model,
                            'modelTaAjuan' => $modelTaAjuan,
                        ]);
                    }
                } else {
                    $model->save();
                }
                return $this->redirect(['index', 'id_ajuan' => $modelTaAjuan->id_ajuan]);
*/
            } else {
                return $this->render('create', [
                    'model' => $model,
                    'modelTaAjuan' => $modelTaAjuan,
                ]);
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Updates an existing TaUjian model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelTaAjuan = TaAjuan::findOne($model['id_ajuan']);

        if($model['detailUjian']['id_penguji'] == Yii::$app->user->id_penguji) {
            if($model->load(Yii::$app->request->post())) {
                if($model->pemberi_hasil == 1) {
                    $modelTaAjuan->load(Yii::$app->request->post());
                    
                    if($modelTaAjuan->hasil_ujian == 'diterima') {
                        TaAjuan::updateAll(
                            ['hasil_ujian' => Yii::$app->params['hasil_ujian'][2]],
                            "hasil_verifikasi = '".Yii::$app->params['hasil_verifikasi'][1]."' AND id_ajuan != '".$modelTaAjuan['id_ajuan']."' AND id_mahasiswa = '".$modelTaAjuan->id_mahasiswa."'"
                        );

                        $update = Yii::$app->db->createCommand('UPDATE ta_ujian LEFT JOIN ta_ajuan ON ta_ajuan.id_ajuan = ta_ujian.id_ajuan SET ta_ujian.pemberi_hasil = 1 WHERE ta_ajuan.id_mahasiswa = '.$modelTaAjuan['id_mahasiswa'])->execute();

                    }
                    $modelTaAjuan->save(false);
                }
                if($model->save()) {
                    return $this->redirect(['index', 'id_ajuan' => $modelTaAjuan['id_ajuan']]);
                }
            } else {
                return $this->render('update', [
                    'model' => $model,
                    'modelTaAjuan' => $modelTaAjuan,
                ]);
            }
        }
    }

    /**
     * Deletes an existing TaUjian model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TaUjian model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TaUjian the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TaUjian::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
