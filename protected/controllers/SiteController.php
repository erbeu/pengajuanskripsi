<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\SignupForm;
use app\models\User;
use app\models\UserAdmin;
use app\models\UserPenguji;
use app\models\UserMahasiswa;
use app\models\Halaman;
use app\models\Pengumuman;
use yii\data\Pagination;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['dashboard', 'logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['dashboard', 'logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['signup'],
                        'allow' => false,
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $this->layout = 'frontend';
        
        $query = Pengumuman::find()->where(['arsip' => 0]);
        $countQuery = clone $query;
        $pages['pengumuman'] = new Pagination(['totalCount' => $countQuery->count(), 'pageSize' => 5]);
        $data['pengumuman'] = $query->offset($pages['pengumuman']->offset)
            ->limit($pages['pengumuman']->limit)
            ->orderBy('id_pengumuman DESC')->asArray()->all();
        
        $query = Halaman::find();
        $countQuery = clone $query;
        $pages['halaman'] = new Pagination(['totalCount' => $countQuery->count(), 'pageSize' => 5]);
        $data['halaman'] = $query->offset($pages['halaman']->offset)
            ->limit($pages['halaman']->limit)
            ->orderBy('id_halaman ASC')->asArray()->all();
        
        return $this->render('index', [
            'data' => $data,
            'pages' => $pages,
        ]);
    }
    public function actionHalaman($id)
    {
        $this->layout = 'frontend';
        $data['halaman'] = Halaman::findOne($id);
        
        return $this->render('halaman', ['data' => $data]);
    }
    public function actionPengumuman($id)
    {
        $this->layout = 'frontend';
        $data['pengumuman'] = Pengumuman::findOne($id);
        
        return $this->render('pengumuman', [
            'data' => $data,
        ]);
    }
    
    public function actionDashboard()
    {
        if(in_array(Yii::$app->user->identity->role, [User::ROLE_ADMIN, User::ROLE_PENGUJI])) {
            $url = ['ta-mahasiswa/index'];
        } else {
            $mahasiswa = UserMahasiswa::findOne(['id_user' => Yii::$app->user->identity->id]);
            $url = ['ta-ajuan/index', 'id_mahasiswa' => $mahasiswa['id_mahasiswa']];
        }
        return $this->redirect($url);
    }
    
    public function actionEditUser()
    {
        $user = User::findOne(Yii::$app->user->identity->id_user);
        $data = [];
        
        if(Yii::$app->user->identity->role == User::ROLE_ADMIN) {
            $model = UserAdmin::findOne(['id_user' => $user['id_user']]);
        } elseif(Yii::$app->user->identity->role == User::ROLE_PENGUJI) {
            $model = UserPenguji::findOne(['id_user' => $user['id_user']]);
        } elseif(Yii::$app->user->identity->role == User::ROLE_MAHASISWA) {
            $model = UserMahasiswa::findOne(['id_user' => $user['id_user']]);
            for($i = date('Y'); $i >= (date('Y')-10); $i--) { $data['list_tahun'][] = $i; }
        }

        if ($model->load(Yii::$app->request->post()) && $user->load(Yii::$app->request->post())) {
            if(!empty($user->pass)) {
                $user->setPassword($user->pass);
                $user->generateAuthKey();
            }
            if($user->save(false)) {
                $model->save();
                Yii::$app->session->setFlash('success', 'Profil berhasil disimpan.');
                return $this->redirect(['dashboard']);
            } else {
                return $this->render('edit-user-'.Yii::$app->user->identity->role, [
                    'model' => $model,
                    'user' => $user,
                    'data' => $data,
                ]);
            }
        } else {
            return $this->render('edit-user-'.Yii::$app->user->identity->role, [
                'model' => $model,
                'user' => $user,
                'data' => $data,
            ]);
        }
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        $this->layout = 'login';
        if (!\Yii::$app->user->isGuest) {
            return $this->redirect(Yii::$app->user->returnUrl);
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->redirect(Yii::$app->user->loginUrl);
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $this->layout = 'login';
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->redirect(Yii::$app->user->returnUrl);
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }
}
