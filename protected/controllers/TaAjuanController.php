<?php

namespace app\controllers;

use Yii;
use app\models\TaAjuan;
use app\models\TaAjuanSearch;
use app\models\TaVerifikasi;
use app\models\TaUjian;
use app\models\JudulFinal;
use app\models\DetailUjianSearch;
use app\models\User;
use app\models\UserMahasiswa;
use app\components\AccessRule;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * TaAjuanController implements the CRUD actions for TaAjuan model.
 */
class TaAjuanController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'send'],
                        'allow' => true,
                        'roles' => [
                            User::ROLE_MAHASISWA,
                        ],
                    ],
                    [
                        'actions' => ['index', 'view'],
                        'allow' => true,
                        'roles' => [
                            User::ROLE_ADMIN,
                            User::ROLE_PENGUJI,
                        ],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TaAjuan models.
     * @return mixed
     */
    public function actionIndex($id_mahasiswa)
    {
        $searchModel = new TaAjuanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $id_mahasiswa);
        
        $searchModelDetailUjian = new DetailUjianSearch();
        $dataProviderDetailUjian = $searchModelDetailUjian->search(Yii::$app->request->queryParams, $id_mahasiswa);
        
        $modelMahasiswa = UserMahasiswa::findOne($id_mahasiswa);
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'dataProviderDetailUjian' => $dataProviderDetailUjian,
            'modelMahasiswa' => $modelMahasiswa,
        ]);
    }
    
    public function actionSend()
    {
        $jml_ta = TaAjuan::find()
            ->where(['id_mahasiswa' => Yii::$app->user->id_mahasiswa, 'hasil_verifikasi' => ['belum diverifikasi', 'diterima']])
            ->count();
        if($jml_ta == 3) {
            $model = TaAjuan::updateAll(['submit' => 1], ['id_mahasiswa' => Yii::$app->user->id_mahasiswa]);
            if($model) {
                Yii::$app->session->setFlash('warning', 'Judul skripsi berhasil dikirim');
            } else {
                Yii::$app->session->setFlash('danger', 'Judul skripsi gagal dikirim');
            }
        } else {
            Yii::$app->session->setFlash('danger', 'Hayooo ngapain');
        }
        return $this->redirect(['index', 'id_mahasiswa' => Yii::$app->user->id_mahasiswa]);
    }

    /**
     * Displays a single TaAjuan model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
            'modelMahasiswa' => UserMahasiswa::findOne(['id_mahasiswa' => $this->findModel($id)['id_mahasiswa']]),
            'modelVerifikasi' => TaVerifikasi::findOne(['id_ajuan' => $id]),
            'modelUjian' => TaUjian::find()->where(['id_ajuan' => $id])->all(),
        ]);
    }

    /**
     * Creates a new TaAjuan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $modelMahasiswa = UserMahasiswa::findOne(['id_user' => Yii::$app->user->identity->id]);
        $modelTaAjuan = TaAjuan::find()->where(['id_mahasiswa' => $modelMahasiswa['id_mahasiswa'], 'hasil_verifikasi' => 'diterima']);
        if($modelTaAjuan->count() < 3) {
            $model = new TaAjuan();

            if ($model->load(Yii::$app->request->post())) {
                $query = JudulFinal::findBySql("SELECT * FROM judul_final WHERE judul = '".$model->judul_ajuan."'")->count();
                if($query == 0) {
                    $model->hasil_verifikasi = Yii::$app->params['hasil_verifikasi'][0];
                    $model->hasil_ujian = Yii::$app->params['hasil_ujian'][0];
                    $model->id_mahasiswa = $modelMahasiswa['id_mahasiswa'];
                    $model->tgl_ajuan = date('Y-m-d G:i:s');
                    if($model->save()) {
                        return $this->redirect(['view', 'id' => $model->id_ajuan]);
                    } else {
                        print_r($model->getErrors());
                        return $this->render('create', [
                            'model' => $model,
                        ]);
                    }
                } else {
                    $model->addError('judul_ajuan', 'Maaf judul yang anda ajukan sudah pernah diajukan.');
                    return $this->render('create', [
                        'model' => $model,
                    ]);
                }
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        } else {
            return $this->redirect(['index', 'id_mahasiswa' => $id_mahasiswa]);
        }
    }

    /**
     * Updates an existing TaAjuan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if($model->submit == 1) return $this->redirect(['index', 'id_mahasiswa' => $model->id_mahasiswa]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_ajuan]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TaAjuan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TaAjuan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TaAjuan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TaAjuan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
