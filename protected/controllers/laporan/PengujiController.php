<?php

namespace app\controllers\laporan;

use Yii;
use app\models\UserPenguji;
use app\models\UserPengujiSearch;
use app\models\UserMahasiswaSearch;
use app\models\User;
use app\components\AccessRule;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * PengujiController implements the CRUD actions for UserPenguji model.
 */
class PengujiController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'actions' => ['index', 'detail'],
                        'allow' => true,
                        'roles' => [
                            User::ROLE_ADMIN,
                        ],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all UserPenguji models.
     * @return mixed
     */
    public function actionIndex()
    {
        for($i = date('Y'); $i >= (date('Y')-10); $i--) { $data['list_tahun'][] = $i; }
        
        $searchModel = new UserPengujiSearch();
        $dataProvider = $searchModel->searchLaporan(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'data' => $data,
        ]);
    }

    /**
     * Lists all UserPenguji models.
     * @return mixed
     */
    public function actionDetail($id_penguji, $tahun)
    {
        for($i = date('Y'); $i >= (date('Y')-10); $i--) { $data['list_tahun'][] = $i; }
        
        $searchModel = new UserMahasiswaSearch();
        $dataProvider = $searchModel->searchLaporanTa(Yii::$app->request->queryParams, [
            'page' => 'laporan-penguji-detail',
            'id_penguji' => $id_penguji,
            'tahun' => $tahun,
        ]);
        
        $modelPenguji = UserPenguji::findOne($id_penguji);

        return $this->render('detail', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'modelPenguji' => $modelPenguji,
            'tahun' => $tahun,
        ]);
    }
}
