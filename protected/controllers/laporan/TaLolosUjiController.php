<?php

namespace app\controllers\laporan;

use Yii;
use app\models\UserMahasiswa;
use app\models\UserMahasiswaSearch;
use app\models\User;
use app\components\AccessRule;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * TaMahasiswaController implements the CRUD actions for UserMahasiswa model.
 */
class TaLolosUjiController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => [
                            User::ROLE_ADMIN,
                        ],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all UserMahasiswa models.
     * @return mixed
     */
    public function actionIndex()
    {
        for($i = date('Y'); $i >= (date('Y')-10); $i--) { $data['list_tahun'][] = $i; }
        
        $searchModel = new UserMahasiswaSearch();
        $dataProvider = $searchModel->searchLaporanTa(Yii::$app->request->queryParams, ['page' => 'ta-lolos-uji']);
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'data' => $data,
        ]);
    }
}
