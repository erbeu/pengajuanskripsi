<?php

namespace app\controllers;

use Yii;
use app\models\UserMahasiswa;
use app\models\UserMahasiswaSearch;
use app\models\User;
use app\components\AccessRule;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * TaMahasiswaController implements the CRUD actions for UserMahasiswa model.
 */
class TaLolosVerifikasiController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => [
                            User::ROLE_ADMIN,
                        ],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all UserMahasiswa models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserMahasiswaSearch();
        $dataProvider = $searchModel->searchAdminTa(Yii::$app->request->queryParams, ['page' => 'ta-lolos-verifikasi']);
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}
