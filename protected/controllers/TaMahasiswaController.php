<?php

namespace app\controllers;

use Yii;
use app\models\UserMahasiswa;
use app\models\UserMahasiswaSearch;
use app\models\User;
use app\components\AccessRule;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * TaMahasiswaController implements the CRUD actions for UserMahasiswa model.
 */
class TaMahasiswaController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => [
                            User::ROLE_ADMIN,
                            User::ROLE_PENGUJI,
                        ],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all UserMahasiswa models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserMahasiswaSearch();
        if(Yii::$app->user->identity->role == User::ROLE_ADMIN) {
            $ext = !empty(Yii::$app->request->get('page')) ? '-'.Yii::$app->request->get('page') : '';
            $dataProvider = $searchModel->searchAdminTa(Yii::$app->request->queryParams, ['page' => 'ta-mahasiswa'.$ext]);
        } elseif(Yii::$app->user->identity->role == User::ROLE_PENGUJI) {
            $dataProvider = $searchModel->searchPengujiTa(Yii::$app->request->queryParams, ['page' => Yii::$app->request->get('page')]);
        }
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}
