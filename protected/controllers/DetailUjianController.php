<?php

namespace app\controllers;

use Yii;
use app\models\DetailUjian;
use app\models\DetailUjianSearch;
use app\models\TaAjuan;
use app\models\User;
use app\models\UserAdmin;
use app\models\UserPenguji;
use app\models\UserMahasiswa;
use app\components\AccessRule;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;

/**
 * DetailUjianController implements the CRUD actions for DetailUjian model.
 */
class DetailUjianController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'allow' => true,
                        'roles' => [
                            User::ROLE_ADMIN,
                        ],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all DetailUjian models.
     * @return mixed
     */
    public function actionIndex($id_mahasiswa)
    {
        $ta_ajuan = TaAjuan::find()
            ->where(['id_mahasiswa' => $id_mahasiswa, 'hasil_verifikasi' => Yii::$app->params['hasil_verifikasi'][1]])
            ->count();
        if($ta_ajuan == 3) {
            $searchModel = new DetailUjianSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $id_mahasiswa);
            $modelMahasiswa = UserMahasiswa::findOne($id_mahasiswa);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'modelMahasiswa' => $modelMahasiswa
            ]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Displays a single DetailUjian model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $modelMahasiswa = UserMahasiswa::findOne($model['id_mahasiswa']);
        
        return $this->render('view', [
            'model' => $model,
            'modelMahasiswa' => $modelMahasiswa,
        ]);
    }

    /**
     * Creates a new DetailUjian model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id_mahasiswa)
    {
        $model = new DetailUjian();
        if($model->find()->where(['id_mahasiswa' => $id_mahasiswa])->count() < 2) {
            $model->id_mahasiswa = $id_mahasiswa;

            $modelAdmin = UserAdmin::findOne(['id_user' => Yii::$app->user->identity->id_user]);
            $modelMahasiswa = UserMahasiswa::findOne($id_mahasiswa);
            $modelPenguji = UserPenguji::find()->asArray()->all();

            if ($model->load(Yii::$app->request->post())) {
                $model->id_admin = $modelAdmin->id_admin;

                if($model->save()) {
                    return $this->redirect(['index', 'id_mahasiswa' => $model->id_mahasiswa]);
                } else {
                    return $this->render('create', [
                        'model' => $model,
                        'modelMahasiswa' => $modelMahasiswa,
                        'modelPenguji' => $modelPenguji,
                    ]);
                }
            } else {
                return $this->render('create', [
                    'model' => $model,
                    'modelMahasiswa' => $modelMahasiswa,
                    'modelPenguji' => $modelPenguji,
                ]);
            }
        } else {
            return $this->redirect(['index', 'id_mahasiswa' => $id_mahasiswa]);
        }
    }

    /**
     * Updates an existing DetailUjian model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        
        $modelAdmin = UserAdmin::findOne(['id_user' => Yii::$app->user->identity->id_user]);
        $modelMahasiswa = UserMahasiswa::findOne($model->id_mahasiswa);
        $modelPenguji = UserPenguji::find()->asArray()->all();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'id_mahasiswa' => $modelMahasiswa['id_mahasiswa']]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'modelMahasiswa' => $modelMahasiswa,
                'modelPenguji' => $modelPenguji,
            ]);
        }
    }

    /**
     * Deletes an existing DetailUjian model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $id_mahasiswa = $model->id_mahasiswa;
        $model->delete();

        return $this->redirect(['index', 'id_mahasiswa' => $id_mahasiswa]);
    }

    /**
     * Finds the DetailUjian model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DetailUjian the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DetailUjian::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
