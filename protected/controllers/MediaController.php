<?php
namespace app\controllers;

use Yii;
use app\models\User;
use app\models\Media;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\components\AccessRule;

class MediaController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'delete'],
                        'allow' => true,
                        'roles' => [
                            User::ROLE_ADMIN,
                        ],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
    public function actionIndex()
    {

        $data['files'] = FileHelper::findFiles(
            './uploads/',
            [
                'only' => ['*.png', '*.jpg'],
                'recursive' => false,
            ]
        );
        rsort($data['files']);
        return $this->render('index', ['data' => $data]);
    }
    
    public function actionCreate()
    {
        $model = new Media();

        if (Yii::$app->request->isPost) {
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            if ($model->upload()) {
                return $this->redirect(['index']);
            }
        }

        return $this->render('create', ['model' => $model]);
    }
    
    public function actionDelete($filename) {
        $filepath = './uploads/'.$filename;
        
        if(file_exists($filepath)) {
            Yii::$app->session->setFlash('warning', 'File '.$filename.' berhasil dihapus');
            unlink($filepath);
        } else {
            Yii::$app->session->setFlash('error', 'File '.$filename.' tidak ditemukan');
        }
        
        return $this->redirect(['index']);
    }
}