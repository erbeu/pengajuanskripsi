<?php

namespace app\controllers;

use Yii;
use app\models\UserPenguji;
use app\models\UserPengujiSearch;
use app\models\UserMahasiswaSearch;
use app\models\User;
use app\components\AccessRule;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;

/**
 * JudulFinalController implements the CRUD actions for JudulFinal model.
 */
class JadwalUjianController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'actions' => ['index', 'view'],
                        'allow' => true,
                        'roles' => [
                            User::ROLE_ADMIN,
                        ],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all JudulFinal models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserPengujiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single JudulFinal model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $searchModel = new UserMahasiswaSearch();
        $dataProvider = $searchModel->searchAdminTa(Yii::$app->request->queryParams, ['page' => 'jadwal-ujian', 'id_penguji' => $id]);
        
        $modelPenguji = UserPenguji::findOne($id);

        return $this->render('view', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'modelPenguji' => $modelPenguji,
        ]);
    }
}
