<?php

namespace app\controllers;

use Yii;
use app\models\JudulFinal;
use app\models\JudulFinalSearch;
use app\models\User;
use app\models\UserMahasiswa;
use app\components\AccessRule;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;

/**
 * TaAjuanController implements the CRUD actions for TaAjuan model.
 */
class CariJudulController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => [
                            User::ROLE_ADMIN,
                            User::ROLE_MAHASISWA,
                        ],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function actionIndex()
    {
        $data['metode'] = Yii::$app->request->get('metode');
        $data['objek'] = Yii::$app->request->get('objek');
        
        $query = JudulFinal::find();
        $where = '';
        
        if(!empty($data['metode'])) {
            $where .= "judul like '%".$data['metode']."%'";
        }
        if(!empty($data['objek'])) {
            if(!empty($data['metode'])) {
                $where .= ' AND ';
            }
            $where .= "judul like '%".$data['objek']."%'";
        }
        
        $query->where($where);
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'data' => $data,
        ]);
    }
}
