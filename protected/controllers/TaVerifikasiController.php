<?php

namespace app\controllers;

use Yii;
use app\models\TaVerifikasi;
use app\models\TaVerifikasiSearch;
use app\models\TaAjuan;
use app\models\User;
use app\models\UserAdmin;
use app\components\AccessRule;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * TaVerifikasiController implements the CRUD actions for TaVerifikasi model.
 */
class TaVerifikasiController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'actions' => ['create', 'update', 'send'],
                        'allow' => true,
                        'roles' => [
                            User::ROLE_ADMIN,
                        ],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TaVerifikasi models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TaVerifikasiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionSend($id_mahasiswa) {
        $ta = TaAjuan::find()
            ->where(['id_mahasiswa' => $id_mahasiswa, 'hasil_verifikasi' => 'belum diverifikasi'])
            ->count();
        $submit = TaAjuan::find()
            ->joinWith(['taVerifikasi'])
            ->where(['id_mahasiswa' => $id_mahasiswa, 'ta_verifikasi.submit' => 0])
            ->count();
        if($ta == 0 && $submit > 0) {
            $model = Yii::$app->db->createCommand('UPDATE ta_verifikasi LEFT JOIN ta_ajuan ON ta_ajuan.id_ajuan = ta_verifikasi.id_ajuan SET ta_verifikasi.submit = 1 WHERE ta_ajuan.id_mahasiswa = '.$id_mahasiswa)->execute();
            if($model) {
                Yii::$app->session->setFlash('warning', 'Hasil verifikasi berhasil dikirim');
            } else {
                Yii::$app->session->setFlash('danger', 'Hasil verifikasi gagal dikirim');
            }
        } else {
            Yii::$app->session->setFlash('danger', 'Hayooo ngapain');
        }
        return $this->redirect(['ta-ajuan/index', 'id_mahasiswa' => $id_mahasiswa]);
    }

    /**
     * Displays a single TaVerifikasi model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id_ajuan)
    {
        return $this->render('view', [
            'model' => $this->findModel($id_ajuan),
        ]);
    }

    /**
     * Creates a new TaVerifikasi model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id_ajuan)
    {
        $model = new TaVerifikasi();
        $modelTaAjuan = TaAjuan::findOne($id_ajuan);
        
        if($modelTaAjuan->hasil_verifikasi == Yii::$app->params['hasil_verifikasi'][0]) {
            $modelAdmin = UserAdmin::findOne(['id_user' => Yii::$app->user->identity->id]);

            if ($model->load(Yii::$app->request->post()) && $modelTaAjuan->load(Yii::$app->request->post())) {
                $model->id_ajuan = $id_ajuan;
                $model->id_admin = $modelAdmin['id_admin'];
                $model->tgl_verifikasi = date('Y-m-d G:i:s');
                if($model->save() && $modelTaAjuan->save(false)) {
                    return $this->redirect(['ta-ajuan/index', 'id_mahasiswa' => $modelTaAjuan['id_mahasiswa']]);
                } else {
                    return $this->render('create', [
                        'model' => $model,
                        'modelTaAjuan' => $modelTaAjuan,
                        'modelAdmin' => $modelAdmin,
                    ]);
                }
            } else {
                return $this->render('create', [
                    'model' => $model,
                    'modelTaAjuan' => $modelTaAjuan,
                    'modelAdmin' => $modelAdmin,
                ]);
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Updates an existing TaVerifikasi model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id_ajuan)
    {
        $model = $this->findModel($id_ajuan);
        $modelTaAjuan = TaAjuan::findOne($id_ajuan);
        $modelAdmin = UserAdmin::findOne(['id_user' => Yii::$app->user->identity->id]);

        if ($model->load(Yii::$app->request->post()) && $modelTaAjuan->load(Yii::$app->request->post()) && $model->save() && $modelTaAjuan->save()) {
            return $this->redirect(['ta-ajuan/index', 'id_mahasiswa' => $modelTaAjuan['id_mahasiswa']]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'modelTaAjuan' => $modelTaAjuan,
                'modelAdmin' => $modelAdmin,
            ]);
        }
    }

    /**
     * Deletes an existing TaVerifikasi model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TaVerifikasi model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TaVerifikasi the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TaVerifikasi::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
