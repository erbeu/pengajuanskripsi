<?php

return [
    'site_name' => 'STMIK IKMI CIREBON',
    
    'adminEmail' => 'admin@example.com',
	'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    
    'role' => ['admin' => 'admin', 'penguji' => 'penguji', 'mahasiswa' => 'mahasiswa'],
    'list_kelas' => ['RPL', 'TKJ', 'ANIMASI'],
    'list_prodi' => ['TI', 'MI', 'KA'],
    'list_semester' => [1, 2, 3, 4, 5, 6, 7, 8],
    
    'hasil_verifikasi' => ['belum diverifikasi', 'diterima', 'ditolak'],
    'hasil_ujian' => ['belum diuji', 'diterima', 'ditolak'],
];
